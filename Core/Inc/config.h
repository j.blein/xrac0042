/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONFIG_H
#define __CONFIG_H

#ifdef      DEBUG_WITH_UART
#define     dbgprint printfDebug
#else
#define dbgprint(...)
#endif

#define 	PRIORITY_INTERRUPT_DALI_TX		0
#define 	PRIORITY_INTERRUPT_DALI_RX		1

//#define USE_SCHEDULER

#define 	ADDR_DALI_MASTER				63/*TODO */



#define STM32F0_DALI


//#define DALI_MASTER
#define DALI_SLAVE


#endif /* __CONFIG_H */
