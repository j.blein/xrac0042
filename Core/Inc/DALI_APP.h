/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DALI_APP_H
#define __DALI_APP_H

#include "DALI.h"
#include "stdint.h"


#define NB_MAX_GROUPE_BALLAST_CONFIG			16

typedef struct
{
	uint8_t     ucSystemConfigBitStop;
	uint8_t     ucSystemConfigInterPacketRx;
	uint8_t     ucSystemConfigInterFrameTx;
	uint8_t     ucSystemConfigInterPacketTx;
	uint8_t     ucSystemConfigPercentTolerance;
	uint8_t     ucSystemConfigCorrectionOpto;

	uint8_t     ucSystemConfigMsbAddrAutoTimeOut;
	uint8_t     ucSystemConfigLsbAddrAutoTimeOut;

	uint16_t     usSystemConfigMeteringFeedbackPeriod;

	uint8_t     ucSystemConfigValue11;
	uint8_t     ucSystemConfigValue12;
	uint8_t     ucSystemConfigValue13;
	uint8_t     ucSystemConfigValue14;

	uint8_t     ucSystemConfigValue15;
	uint8_t     ucSystemConfigValue16;
	uint8_t     ucSystemConfigValue17;
	uint8_t     ucSystemConfigValue18;

	uint8_t     ucSystemConfigValue19;
	uint8_t     ucSystemConfigValue20;
	uint8_t     ucSystemConfigValue21;
	uint8_t     ucSystemConfigValue22;

	uint8_t     ucSystemConfigValue23;
	uint8_t     ucSystemConfigValue24;
	uint8_t     ucSystemConfigValue25;
}stcSystemConfig_t;

typedef struct
{
	uint8_t     ucDaliGatewayType;
	uint8_t     ucDaliGatewayDtr1;
	uint8_t     ucDaliGatewayDtr0;
	uint8_t     ucDaliGatewayAddr;
	uint8_t     ucDaliGatewayLsb;
	uint8_t     ucDaliGatewayMsb;

}stcRuntimeDaliGateway_t;

typedef struct
{
	uint8_t ucID1;
	uint8_t ucID2;
}stcDaliCnfBalIdInverGrp_t;

typedef struct
{
	uint8_t ucGrp;               /*Group to add the ballasts below */
	uint8_t ucIDs_a[NB_MAX_GROUPE_BALLAST_CONFIG];
}stcDaliCnfBalAddDelGrp_t;


typedef struct
{
	uint8_t ucStateTestBallast;
	uint8_t ucBallastAddress;
}stcDaliCnfTestBallast_t;

typedef struct
{
	uint8_t ucStateTestBallast;
	uint8_t ucGroupeAddress;
}stcDaliCnfTestGroupe_t;

//void DaliConfigRemoteControllerBallastTest(stcDaliGenericRxFrame_t *stcDaliGenericRxFrame_pt);
void SetTestDaliBallastRuntime(stcDaliCnfTestBallast_t *stcDaliCnfTestBallast_pt);
void SetTestDaliGroupeRuntime(stcDaliCnfTestGroupe_t *stcDaliCnfTestGroupe_pt);

#endif /* __DALI_APP_H */
