/**
 * @file utilities.h
 * @author R&D Team (arcom@groupe-arcom.com)
 * @brief 
 * @version 1.0
 * @date 27-01-2020
 * 
 * @copyright Copyright Arcom 2020
 * 
 */

#ifndef UTILITIES_H /** Prevent multiple inclusions **/
#define UTILITIES_H

#include <stdint.h>
#include <stdbool.h>
/* Exported types---------------------------------------------*/


/* Exported constants---------------------------------------------*/


/** Exported functions---------------------------------------------*/
void Error_Handler(void);
bool Buffercmp(uint8_t *pBuffer1, uint8_t *pBuffer2, uint16_t BufferLength);
void memcpyST( uint8_t *ucDst_p, const uint8_t *ucSrc_p, uint16_t usSize );
uint16_t CalcCrc16Frame(uint8_t *uc_p, uint16_t usFrSize);
bool getMyEndianness(void);
void AddCrc16ToFrame(uint8_t * ucData_p,uint16_t usSize);
#endif /*UTILITIES_H*/
