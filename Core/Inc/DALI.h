/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DALI_H
#define __DALI_H

#include "config.h" /*#define AIO_DALI*/
#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_gpio.h"
#include "DALI_APP.h"


#ifdef	AIO_DALI



	#define	CORRECTION_OPTO_DALI							GetConfigDali(CONFIG_DALI_CORRECTION_OPTO)/*correction in µs for latency of opto*/

	#define USER_PIN_RX_DALI_HIGH							0
	#define	USER_PIN_RX_DALI_LOW							1

	#define GPIO_DALI_TX_PORT								GPIOD
	#define GPIO_DALI_TX_PIN								GPIO_PIN_6

	#define GPIO_DALI_RX_PORT								GPIOB
	#define GPIO_DALI_RX_PIN								GPIO_PIN_3

	#define	GPOI_DALI_TX_ENABLE_CLK_PORT					__HAL_RCC_GPIOD_CLK_ENABLE()
	#define	GPOI_DALI_RX_ENABLE_CLK_PORT					__HAL_RCC_GPIOB_CLK_ENABLE()

	#define	VOID_FUNCTION_PIN_RX_DALI_INTERRUPT_VOID		void EXTI2_3_IRQHandler(void)
	#define	GPIO_EXTI_DALI_RX								EXTI2_3_IRQn

	#define	VOID_FUNCTION_IRQ_DALI_FRONT_TRANSITION_TX_VOID	void TIM6_IRQHandler(void)
	#define	TIM_DALI_FRONT_TRANSITION_TX_INSTANCE			TIM6
	#define	TIM_DALI_FRONT_TRANSITION_TX_PRESCALER			63/*((freq of internal clock) / (freq we want to count)) -1; (64000000/1000000)-1 = 63*/

	#define	TIM_DALI_CPT_BETWEEN_FRONT_INSTANCE				TIM3
	#define	TIM_DALI_CPT_BETWEEN_FRONT_PRESCALER			63/*((freq of internal clock) / (freq we want to count)) -1; (64000000/1000000)-1 = 63*/

	#define TX_DALI_LOW()           						HAL_GPIO_WritePin(GPIO_DALI_TX_PORT, GPIO_DALI_TX_PIN, GPIO_PIN_SET)
	#define TX_DALI_HIGH()          						HAL_GPIO_WritePin(GPIO_DALI_TX_PORT, GPIO_DALI_TX_PIN, GPIO_PIN_RESET)

/* Ajout JBL */
#define __HAL_GPIO_EXTI_GET_RISING_IT void TEST(void)
#define __HAL_GPIO_EXTI_CLEAR_RISING_IT void TEST(void)
#define HAL_GPIO_EXTI_Rising_Callback void TEST(void)
#define __HAL_GPIO_EXTI_GET_FALLING_IT void TEST(void)
#define __HAL_GPIO_EXTI_CLEAR_FALLING_IT void TEST(void)
#define HAL_GPIO_EXTI_Falling_Callback void TEST(void)


/*
 *
 * Attention chercher la fonction void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base) pour integrer :
 * if(htim_base->Instance==TIM3)
 * {
 * 	__HAL_RCC_TIM3_CLK_ENABLE();
 * }
 *
 */
/*
 *
 * Attention chercher ce define pour le mettre à un #define USE_HAL_TIM_REGISTER_CALLBACKS    1u
 *
 */
#endif
#ifdef	STM32F0_DALI

	#define	CORRECTION_OPTO_DALI							GetConfigDali(CONFIG_DALI_CORRECTION_OPTO)/*correction in µs for latency of opto*/

	#define USER_PIN_RX_DALI_HIGH							0
	#define	USER_PIN_RX_DALI_LOW							1

	#define GPIO_DALI_TX_PORT								GPIOB
	#define GPIO_DALI_TX_PIN								GPIO_PIN_14

	#define GPIO_DALI_RX_PORT								GPIOB
	#define GPIO_DALI_RX_PIN								GPIO_PIN_13

	#define	GPOI_DALI_TX_ENABLE_CLK_PORT					__HAL_RCC_GPIOB_CLK_ENABLE()
	#define	GPOI_DALI_RX_ENABLE_CLK_PORT					__HAL_RCC_GPIOB_CLK_ENABLE()

	#define	VOID_FUNCTION_PIN_RX_DALI_INTERRUPT_VOID		void EXTI2_3_IRQHandler(void)
	#define	GPIO_EXTI_DALI_RX								EXTI2_3_IRQn

	#define	VOID_FUNCTION_IRQ_DALI_FRONT_TRANSITION_TX_VOID	void TIM1_BRK_UP_TRG_COM_IRQHandler(void)
	#define	TIM_DALI_FRONT_TRANSITION_TX_INSTANCE			TIM1
	#define	TIM_DALI_FRONT_TRANSITION_TX_PRESCALER			47/*((freq of internal clock) / (freq we want to count)) -1; (64000000/1000000)-1 = 63*/

	#define	TIM_DALI_CPT_BETWEEN_FRONT_INSTANCE				TIM3
	#define	TIM_DALI_CPT_BETWEEN_FRONT_PRESCALER			63/*((freq of internal clock) / (freq we want to count)) -1; (64000000/1000000)-1 = 63*/

	#define TX_DALI_LOW()           						HAL_GPIO_WritePin(GPIO_DALI_TX_PORT, GPIO_DALI_TX_PIN, GPIO_PIN_SET)
	#define TX_DALI_HIGH()          						HAL_GPIO_WritePin(GPIO_DALI_TX_PORT, GPIO_DALI_TX_PIN, GPIO_PIN_RESET)

/* Ajout JBL */

#define __HAL_GPIO_EXTI_GET_RISING_IT(__EXTI_LINE__) __HAL_GPIO_EXTI_GET_IT(__EXTI_LINE__)
#define __HAL_GPIO_EXTI_CLEAR_RISING_IT(__EXTI_LINE__) __HAL_GPIO_EXTI_CLEAR_IT(__EXTI_LINE__)
#define __HAL_GPIO_EXTI_GET_FALLING_IT(__EXTI_LINE__) __HAL_GPIO_EXTI_GET_IT(__EXTI_LINE__)
#define __HAL_GPIO_EXTI_CLEAR_FALLING_IT(__EXTI_LINE__) __HAL_GPIO_EXTI_CLEAR_IT(__EXTI_LINE__)


#define HAL_GPIO_EXTI_Rising_Callback(GPIO_Pin)  HAL_GPIO_EXTI_Callback(GPIO_Pin)
#define HAL_GPIO_EXTI_Falling_Callback(GPIO_Pin) HAL_GPIO_EXTI_Callback(GPIO_Pin)


/*
 *
 * Attention chercher la fonction void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base) pour integrer :
 * if(htim_base->Instance==TIM3)
 * {
 * 	__HAL_RCC_TIM3_CLK_ENABLE();
 * }
 *
 */
/*
 *
 * Attention chercher ce define pour le mettre à un #define USE_HAL_TIM_REGISTER_CALLBACKS    1u
 *
 */
#endif

#define CONFIG_DALI_BIT_STOP							0
#define CONFIG_DALI_INTER_PACKET_RX						1
#define CONFIG_DALI_INTER_FRAME_TX						2
#define CONFIG_DALI_INTER_PACKET_TX						3
#define CONFIG_DALI_PERCENT_TOLERANCE					4
#define CONFIG_DALI_CORRECTION_OPTO						5

#define DALI_FRONT_UP									1
#define DALI_FRONT_DOWN									0

#define USER_TRUE										1
#define	USER_FALSE										0

#define USER_NO_VALUE									0

#define	PERCENTAGE_TOLERANCE							GetConfigDali(CONFIG_DALI_PERCENT_TOLERANCE)/*value of tolerance between 2front in % (12% indication from Norme)*/

#define	TIM_DALI_BIT_STOP_PERIOD						GetConfigDali(CONFIG_DALI_BIT_STOP)/*ms*/
#define	TIM_DALI_INTER_TRAM_PERIOD						GetConfigDali(CONFIG_DALI_INTER_PACKET_RX)/*ms*/
#define	TIM_DALI_INTER_FRAME_TX_PERIOD					GetConfigDali(CONFIG_DALI_INTER_FRAME_TX)/*ms*/
#define	TIM_DALI_INTER_PACKET_TX_PERIOD					GetConfigDali(CONFIG_DALI_INTER_PACKET_TX)/*ms*/

#define	TIM_DALI_FRONT_TRANSITION_TX_PERIOD				416/*�s*/

#define START_BIT_DELAY									416/*�s*/
#define OTHER_BIT_DELAY									833/*�s*/

#define NUMBER_TIMER_DALI_MS							4/*if you change this number, you have to had callback in fonction TIMERDALI scheduler*/
#define	TIMER_DALI_BIT_STOP								0
#define	TIMER_DALI_INTER_TRAM							1
#define	TIMER_DALI_INTER_FRAME_TX						2
#define	TIMER_DALI_INTER_PACKET_TX						3

#define START_RESET_TIMER								1
#define STOP_TIMER										0
#define PROCESSING_TIMER								2
#define CHECK_ELAPSE_TIMER								1
#define CHANGE_STATE_TIMER								0

#define NUMBER_MAX_FRAME								6
#define STOP_LECTURE									0xFF

#define	CMD_FIRST_CFG_CMD								32 /* Si commande dali entre ces 2 valeur --> l'emettre 2 fois*/
#define	CMD_LAST_CFG_CMD								143/* Si commande dali entre ces 2 valeur --> l'emettre 2 fois*/
#define CMD_DALI_227									0xE3		//cmd sp�cial philips
#define ADRESSE_BROADCAST_DALI_CMD 						0xFF	// direct arc power level following; broadcast address

#define DTR1_DALI_RX_INDEX								0
#define DTR_DALI_RX_INDEX								1
#define ADDR_DALI_RX_INDEX								2
#define LSB_DALI_RX_INDEX								3
#define MSB_DALI_RX_INDEX								4

#define SHORT_RESPONSE_TYPE_1							2
#define SHORT_RESPONSE_TYPE_2							3
#define SHORT_RESPONSE_TYPE_3							4

#define SIZE_FRAME_DALI									4
#define NB_BITS_FRAME_DALI_REQUEST						19
#define SIZE_DALI_TX_FRAME								sizeof(stcDaliTxFrame_t)
#define SIZE_DALI_RX_FRAME								sizeof(stcDaliGenericRxFrame_t)


#define NB_FRAME_MAX									5



//NATURES DES COMMANDES DALI envoy�es en stcDali[ucDali].ucBufferEmissionDali_a[3]
//---------------------------------------
// Commandes Dali:
#define CMD_DALI_SET_OFF					0x00		// 0
#define CMD_DALI_UP							0x01		// 1
#define CMD_DALI_DOWN						0x02		// 2
#define CMD_DALI_SET_UP						0x03		// 3
#define CMD_DALI_Set_DOWN					0x04		// 4
#define CMD_DALI_RECALL_MAX_LEVEL			0x05		// 5
#define CMD_DALI_RECALL_MIN_LEVEL			0x06		// 6
#define CMD_DALI_RECALL_SETDOWN_AND_OFF		0x07		// 7
#define CMD_DALI_RECALL_ON_AND_SETUP		0x08		// 8
#define CMD_DALI_ENABLEDAPC_SEQ				0x09		// 9
//10-15 reserved
#define CMD_DALI_GOTO_SCENE0				0x10		// 16
#define CMD_DALI_GOTO_SCENE1				0x11		// 17
#define CMD_DALI_GOTO_SCENE2				0x12		// 18
#define CMD_DALI_GOTO_SCENE3				0x13		// 19
#define CMD_DALI_GOTO_SCENE4				0x14		// 20
#define CMD_DALI_GOTO_SCENE5				0x15		// 21
#define CMD_DALI_GOTO_SCENE6				0x16		// 22
#define CMD_DALI_GOTO_SCENE7				0x17		// 23
#define CMD_DALI_GOTO_SCENE8				0x18		// 24
#define CMD_DALI_GOTO_SCENE9				0x19		// 25
#define CMD_DALI_GOTO_SCENE10				0x1A		// 26
#define CMD_DALI_GOTO_SCENE11				0x1B		// 27
#define CMD_DALI_GOTO_SCENE12				0x1C		// 28
#define CMD_DALI_GOTO_SCENE13				0x1D		// 29
#define CMD_DALI_GOTO_SCENE14				0x1E		// 30
#define CMD_DALI_GOTO_SCENE15				0x1F		// 31
#define CMD_DALI_RESET						0x20		// 32
#define CMD_DALI_STORE_ACTUEL_LVL_IN_DTR	0x21		// 33
//34-41 reserved
#define CMD_DALI_STORE_DTR_AS_MAX_LVL		0x2A		// 42
#define CMD_DALI_STORE_DTR_AS_MIN_LVL		0x2B		// 43
#define CMD_DALI_STORE_DTR_AS_SYS_FAIL		0x2C		// 44
#define CMD_DALI_STORE_DTR_AS_POW_ON		0x2D		// 45
#define CMD_DALI_STORE_DTR_AS_FADE_TIME		0x2E		// 46
#define CMD_DALI_STORE_DTR_AS_FADE_RATE		0x2F		// 47
//48-63 reserved
#define CMD_DALI_STORE_DTR_AS_SCENE0		0x40		// 64
#define CMD_DALI_STORE_DTR_AS_SCENE1		0x41		// 65
#define CMD_DALI_STORE_DTR_AS_SCENE2		0x42		// 66
#define CMD_DALI_STORE_DTR_AS_SCENE3		0x43		// 67
#define CMD_DALI_STORE_DTR_AS_SCENE4		0x44		// 68
#define CMD_DALI_STORE_DTR_AS_SCENE5		0x45		// 69
#define CMD_DALI_STORE_DTR_AS_SCENE6		0x46		// 70
#define CMD_DALI_STORE_DTR_AS_SCENE7		0x47		// 71
#define CMD_DALI_STORE_DTR_AS_SCENE8		0x48		// 72
#define CMD_DALI_STORE_DTR_AS_SCENE9		0x49		// 73
#define CMD_DALI_STORE_DTR_AS_SCENE10		0x4A		// 74
#define CMD_DALI_STORE_DTR_AS_SCENE11		0x4B		// 75
#define CMD_DALI_STORE_DTR_AS_SCENE12		0x4C		// 76
#define CMD_DALI_STORE_DTR_AS_SCENE13		0x4D		// 77
#define CMD_DALI_STORE_DTR_AS_SCENE14		0x4E		// 78
#define CMD_DALI_STORE_DTR_AS_SCENE15		0x4F		// 79
#define CMD_DALI_REMOVE_FROM_SCENE0			0x50		// 80
#define CMD_DALI_REMOVE_FROM_SCENE1			0x51		// 81
#define CMD_DALI_REMOVE_FROM_SCENE2			0x52		// 82
#define CMD_DALI_REMOVE_FROM_SCENE3			0x53		// 83
#define CMD_DALI_REMOVE_FROM_SCENE4			0x54		// 84
#define CMD_DALI_REMOVE_FROM_SCENE5			0x55		// 85
#define CMD_DALI_REMOVE_FROM_SCENE6			0x56		// 86
#define CMD_DALI_REMOVE_FROM_SCENE7			0x57		// 87
#define CMD_DALI_REMOVE_FROM_SCENE8			0x58		// 88
#define CMD_DALI_REMOVE_FROM_SCENE9			0x59		// 89
#define CMD_DALI_REMOVE_FROM_SCENE10		0x5A		// 90
#define CMD_DALI_REMOVE_FROM_SCENE11		0x5B		// 91
#define CMD_DALI_REMOVE_FROM_SCENE12		0x5C		// 92
#define CMD_DALI_REMOVE_FROM_SCENE13		0x5D		// 93
#define CMD_DALI_REMOVE_FROM_SCENE14		0x5E		// 94
#define CMD_DALI_REMOVE_FROM_SCENE15		0x5F		// 95
#define CMD_DALI_ADD_TO_GROUP0				0x60		// 96
#define CMD_DALI_ADD_TO_GROUP1				0x61		// 97
#define CMD_DALI_ADD_TO_GROUP2				0x62		// 98
#define CMD_DALI_ADD_TO_GROUP3				0x63		// 99
#define CMD_DALI_ADD_TO_GROUP4				0x64		// 100
#define CMD_DALI_ADD_TO_GROUP5				0x65		// 101
#define CMD_DALI_ADD_TO_GROUP6				0x66		// 102
#define CMD_DALI_ADD_TO_GROUP7				0x67		// 103
#define CMD_DALI_ADD_TO_GROUP8				0x68		// 104
#define CMD_DALI_ADD_TO_GROUP9				0x69		// 105
#define CMD_DALI_ADD_TO_GROUP10				0x6A		// 106
#define CMD_DALI_ADD_TO_GROUP11				0x6B		// 107
#define CMD_DALI_ADD_TO_GROUP12				0x6C		// 108
#define CMD_DALI_ADD_TO_GROUP13				0x6D		// 109
#define CMD_DALI_ADD_TO_GROUP14				0x6E		// 110
#define CMD_DALI_ADD_TO_GROUP15				0x6F		// 111
#define CMD_DALI_REMOVE_FROM_GROUP0			0x70		// 112
#define CMD_DALI_REMOVE_FROM_GROUP1			0x71		// 113
#define CMD_DALI_REMOVE_FROM_GROUP2			0x72		// 114
#define CMD_DALI_REMOVE_FROM_GROUP3			0x73		// 115
#define CMD_DALI_REMOVE_FROM_GROUP4			0x74		// 116
#define CMD_DALI_REMOVE_FROM_GROUP5			0x75		// 117
#define CMD_DALI_REMOVE_FROM_GROUP6			0x76		// 118
#define CMD_DALI_REMOVE_FROM_GROUP7			0x77		// 119
#define CMD_DALI_REMOVE_FROM_GROUP8			0x78		// 120
#define CMD_DALI_REMOVE_FROM_GROUP9			0x79		// 121
#define CMD_DALI_REMOVE_FROM_GROUP10		0x7A		// 122
#define CMD_DALI_REMOVE_FROM_GROUP11		0x7B		// 123
#define CMD_DALI_REMOVE_FROM_GROUP12		0x7C		// 124
#define CMD_DALI_REMOVE_FROM_GROUP13		0x7D		// 125
#define CMD_DALI_REMOVE_FROM_GROUP14		0x7E		// 126
#define CMD_DALI_REMOVE_FROM_GROUP15		0x7F		// 127
#define CMD_DALI_STORE_DTR_AS_SHORT_ADR		0x80		// 128
#define CMD_DALI_ENABLE_WRITE_MEMORY		0x81		// 129
//130-143 reserved
#define CMD_DALI_QUERY_STATUS				0x90		// 144
#define CMD_DALI_QUERY_CTRL_GEAR			0x91		// 145
#define CMD_DALI_QUERY_LAMP_FAIL			0x92		// 146
#define CMD_DALI_QUERY_LAMP_PWR_ON			0x93		// 147
#define CMD_DALI_QUERY_LIMIT_ERROR			0x94		// 148
#define CMD_DALI_QUERY_RESET_STATE			0x95		// 149
#define CMD_DALI_QUERY_MISS_SHORT_ADR		0x96		// 150
#define CMD_DALI_QUERY_VERSION_NUMBER		0x97		// 151
#define CMD_DALI_QUERY_CONTENT_DTR			0x98		// 152
#define CMD_DALI_QUERY_DEVICE_TYPE			0x99		// 153
#define CMD_DALI_QUERY_PHYS_MIN_LVL			0x9A		// 154
#define CMD_DALI_QUERY_PWR_FAILURE			0x9B		// 155
#define CMD_DALI_QUERY_CONTENT_DTR1			0x9C		// 156
#define CMD_DALI_QUERY_CONTENT_DTR2			0x9D		// 157
//158-159
#define CMD_DALI_QUERY_ACTUAL_LVL			0xA0		// 160
#define CMD_DALI_QUERY_MAX_LVL				0xA1		// 161
#define CMD_DALI_QUERY_MIN_LVL				0xA2		// 162
#define CMD_DALI_QUERY_PWR_ON_LVL			0xA3		// 163
#define CMD_DALI_QUERY_SYS_FAIL_LVL			0xA4		// 164
#define CMD_DALI_QUERY_FADE_TIME_RATE		0xA5		// 165
//166-175
#define CMD_DALI_QUERY_SCENE0_LVL			0xB0		// 176
#define CMD_DALI_QUERY_SCENE1_LVL			0xB1		// 177
#define CMD_DALI_QUERY_SCENE2_LVL			0xB2		// 178
#define CMD_DALI_QUERY_SCENE3_LVL			0xB3		// 179
#define CMD_DALI_QUERY_SCENE4_LVL			0xB4		// 180
#define CMD_DALI_QUERY_SCENE5_LVL			0xB5		// 181
#define CMD_DALI_QUERY_SCENE6_LVL			0xB6		// 182
#define CMD_DALI_QUERY_SCENE7_LVL			0xB7		// 183
#define CMD_DALI_QUERY_SCENE8_LVL			0xB8		// 184
#define CMD_DALI_QUERY_SCENE9_LVL			0xB9		// 185
#define CMD_DALI_QUERY_SCENE10_LVL			0xBA		// 186
#define CMD_DALI_QUERY_SCENE11_LVL			0xBB		// 187
#define CMD_DALI_QUERY_SCENE12_LVL			0xBC		// 188
#define CMD_DALI_QUERY_SCENE13_LVL			0xBD		// 189
#define CMD_DALI_QUERY_SCENE14_LVL			0xBE		// 190
#define CMD_DALI_QUERY_SCENE15_LVL			0xBF		// 191
#define	CMD_DALI_QUERY_GROUP_0_7			0xC0		// 192
#define	CMD_DALI_QUERY_GROUP_8_15			0xC1		// 193
#define	CMD_DALI_QUERY_RANDOM_ADRH			0xC2		// 194
#define	CMD_DALI_QUERY_RANDOM_ADRM			0xC3		// 195
#define	CMD_DALI_QUERY_RANDOM_ADRL			0xC4		// 196
#define	CMD_DALI_READ_MEMORY				0xC5		// 197

#define CMD_DALI_227						0xE3		//cmd sp�cial philips
#define CMD_DALI_238						0xEE		//cmd sp�cial philips
//198-223	reserved
//224-254	extended
#define	CMD_DALI_QUERY_EXT_VERS_NUMBER		0xFF		// 255


//seconde partie les num correspondent � la premi�re partie du message
#define CMD_DALI_TERMINATE					0xA1		// 256: 1010 0001 0000 0000
#define CMD_DALI_STORE_IN_DTR				0xA3		// 163	//257
#define CMD_DALI_INITIALIZE					0xA5		// 258: 1010 0101 0000 0000
#define CMD_DALI_RANDOMIZE					0xA7		// 259: 1010 0111 0000 0000
#define CMD_DALI_COMPARE					0xA9		// 260: 1010 1001 0000 0000
#define CMD_DALI_WITHDRAW					0xAB		// 261: 1010 1011 0000 0000
//262-263 reserved
#define CMD_DALI_SEARCHADDRH				0xB1		// 264: 1011 0001 HHHH HHHH
#define CMD_DALI_SEARCHADDRM				0xB3		// 265: 1011 0011 MMMM MMMM
#define CMD_DALI_SEARCHADDRL				0xB5		// 266: 1011 0101 LLLL LLLL
#define CMD_DALI_PROGRAM_SHORT_ADDRESS		0xB7		// 267: 1011 0111 0AAA AAA1
#define CMD_DALI_VERIFY_SHORT_ADDRESS		0xB9		// 268: 1011 1001 0AAA AAA1
#define CMD_DALI_QUERY_SHORT_ADDRESS		0xBB		// 269: 1011 1011 0000 0000
#define CMD_DALI_PHYS_SELECTION				0xBD		// 270: 1011 1101 0000 0000
//271 reserved
#define CMD_DALI_ENABLE_DEVICE				0xC1		//272
#define CMD_DALI_STORE_IN_DTR1				0xC3		//273
#define CMD_DALI_STORE_IN_DTR2				0xC5		//274
#define CMD_DALI_WRITE_MEMORY				0xC7		//275
//276-349 reserved
#define CMD_DALI_BLOCAGE					104

#define ADRESSAUTO	1
#define CHGBALLAST	2
#define ADRESSGRP	3

#define DALI_GATEWAY_TYPE_BALLAST	0
#define DALI_GATEWAY_TYPE_ACC		1

#define DALI_LINE_OK		1
#define DALI_LINE_NOK		0


#define TIME_ACTIVATION_RELAY_230	500/*ms*/


typedef enum  {
    DALI_FSM_WATCH_LINE_INIT,
	DALI_FSM_WATCH_LINE_FIRST_CHECK,
	DALI_FSM_WATCH_LINE_OK,
	DALI_FSM_WATCH_LINE_DO_NOTHING
} enDaliFsmWatchLineState_t;

typedef enum  {
	DALI_BALLAST_CONF_STANDBY,
	DALI_BALLAST_CONF_ADDR_AUTO_TYPE,
	DALI_BALLAST_CONF_INVERSION_TYPE,
	DALI_BALLAST_CONF_REPLACE_TYPE,
	DALI_BALLAST_CONF_ADD_GRP_TYPE,
	DALI_BALLAST_CONF_DEL_ADDR_TYPE,
	DALI_BALLAST_CONF_QUERY_TYPE,
	DALI_BALLAST_CONF_TEST_BALLAST_TYPE,
	DALI_BALLAST_CONF_TEST_GROUPE_TYPE,
} enDaliBallastConfigType_t;

typedef enum  {
    DALI_RX_FSM_INIT,
	DALI_RX_FSM_ATTENTE,
	DALI_RX_FSM_START,
	DALI_RX_FSM_BIT_DECODER,
	DALI_RX_FSM_ADDR_AUTO,
	DALI_RX_FSM_CALIBRATION,
	DALI_RX_FSM_ERREUR,
	DALI_RX_FSM_SHORT_CIRCUIT
} enDaliRxFsmState_t;

typedef enum  {
	DALI_RX_FSM_B0,
	DALI_RX_FSM_B1,
	DALI_RX_FSM_B2,
	DALI_RX_FSM_B3,
	DALI_RX_FSM_B4,
	DALI_RX_FSM_B5,
	DALI_RX_FSM_B6,
	DALI_RX_FSM_B7,
	DALI_RX_FSM_B8,
	DALI_RX_FSM_B9,
	DALI_RX_FSM_B10,
	DALI_RX_FSM_B11,
	DALI_RX_FSM_B12,
	DALI_RX_FSM_B13,
	DALI_RX_FSM_B14,
	DALI_RX_FSM_B15,
	DALI_RX_FSM_B16,
	DALI_RX_FSM_B17,
	DALI_RX_FSM_B18,
	DALI_RX_FSM_B19,
	DALI_RX_FSM_B20,
	DALI_RX_FSM_B21,
	DALI_RX_FSM_B22,
	DALI_RX_FSM_B23,
	DALI_RX_FSM_B24

} enDaliRxFsmBitDecoder_t;


typedef union
{
    uint8_t ucBuffer[SIZE_FRAME_DALI];				//3 Octets de data + Start et stop
	struct
    {
		// Reponse Dali sur 1 Octet:
		// l'ordre est primordial, � savoir dans un long int, le LSByte est � l'adresse Buffer+0
        //                                                       MSByte                 Buffer+3
        //                                  dans chaque Byte, le LSBit  est le 1� bit d�clar�
        //                                                       MSBit         8�
    	uint8_t ReponseMsb:7;   //Buffer[0]
    	uint8_t Start:1;
    	uint8_t :5;   			 //Buffer[1]
    	uint8_t Stop:2;
    	uint8_t ReponseLsb:1;
    	uint8_t :8;   			 //Buffer[2]
    	uint8_t TypeTrame:2;    //Buffer[3]
    	uint8_t :6;
    }Response;
	struct
    {
		// Requete Dali sur 2 Octet:
		// l'ordre est primordial, � savoir dans un long int, le LSByte est � l'adresse Buffer+0
        //                                                       MSByte                 Buffer+3
        //                                  dans chaque Byte, le LSBit  est le 1� bit d�clar�
        //                                                       MSBit         8�
#if 0
		uint8_t Start:1;       //Buffer[0]
		uint8_t AdresseMsb:7;   
    	
		uint8_t AdresseLsb:1;   //Buffer[1]
    	uint8_t CommandeMsb:7;  
    	
		uint8_t CommandeLsb:1;  //Buffer[2]
		uint8_t Stop:2;
    	uint8_t :5;   			 
    	
    	
    	uint8_t TypeTrame:2;    //Buffer[3]
    	uint8_t :6;
#endif
    	uint8_t AdresseMsb:7;   //Buffer[0]
    	uint8_t Start:1;
    	uint8_t CommandeMsb:7;  //Buffer[1]
    	uint8_t AdresseLsb:1;
    	uint8_t :5;   			 //Buffer[2]
    	uint8_t Stop:2;
    	uint8_t CommandeLsb:1;
    	uint8_t TypeTrame:2;    //Buffer[3]
    	uint8_t :6;

    }Request;
	// Requete Dali sur 3 Octets: pr�vut pour evolution DALI 2
}unFrameDali_t;

typedef union
{
    uint32_t uiAddr;  	// soit 4 octets = 32 bits
    struct
    {
        // l'ordre est primordial, � savoir dans un long int, le LSByte est � l'adresse Buffer+0
        //                                                       MSByte                 Buffer+3
        //                                  dans chaque Byte, le LSBit  est le 1� bit d�clar�
        //                                                       MSBit         8�
    	uint8_t ucL:8;			//LSB
    	uint8_t ucM:8;
    	uint8_t ucH:8;
    	uint8_t ucNotUsed:8;	//MSB
	}Bytes;
}unSearchAddress_t;

typedef enum  {
    DALI_TX_FSM_INIT,
	DALI_TX_FSM_WAIT_FIFO,
	DALI_TX_FSM_IS_READY_TX,
	DALI_TX_FSM_PARSE_DATA,
	DALI_TX_FSM_BUILD_FRAME,
	DALI_TX_FSM_WAIT_FOR_END_TX,
	DALI_TX_FSM_WAIT_FOR_TX_VALIDATION,
	DALI_TX_FSM_WAIT_FOR_END_TIMER,
	DALI_TX_FSM_SHORT_CIRUIT
} enDaliTxFsmState_t;


typedef enum  {
    DALI_TX_PARSE_STATE_CMD_BALLAST,
	DALI_TX_PARSE_STATE_INTER_FRAME,
	DALI_TX_PARSE_STATE_CMD_COMPARE,
	DALI_TX_PARSE_STATE_END
} enDaliTxFrameType_t;

typedef struct
{
	uint8_t ucDtr1;
	uint8_t ucDtr;
	uint8_t	ucCmd;
	uint8_t ucLsb;
	uint8_t ucMsb;
	uint8_t ucAdresse;
}stcDaliTxFrame_t;


typedef enum  {
    DALI_FSM_ADR_AUTO_INIT,
	DALI_FSM_ADR_AUTO_OFF,
	DALI_FSM_ADR_AUTO_LOCK_ACC,
	DALI_FSM_ADR_AUTO_REMOVE_GROUPS,
	DALI_FSM_ADR_AUTO_RESET,
	DALI_FSM_ADR_AUTO_INITIALIZE,
	DALI_FSM_ADR_AUTO_ADR_FACTORY,
	DALI_FSM_ADR_AUTO_RANDOMIZE,
	DALI_FSM_ADR_AUTO_INIT_BISECTION,
	DALI_FSM_ADR_AUTO_BISECTION,
	DALI_FSM_ADR_AUTO_COMPARE,
	DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_COMPARE,
	DALI_FSM_ADR_AUTO_PROGRAM_SHORT_ADDR,
	DALI_FSM_ADR_AUTO_VERIFY_SHORT_ADDR,
	DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_SHORT_ADDR,
	DALI_FSM_ADR_AUTO_WITHDRAW,
	DALI_FSM_ADR_AUTO_COMPAREW,
	DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_WITHDRAW,
	DALI_FSM_ADR_AUTO_TERMINATE,
	DALI_FSM_ADR_AUTO_STANDBY,
} enDaliFsmAdrState_t;

typedef enum  {
    DALI_FSM_REPL_INIT,
	DALI_FSM_REPL_OFF,
	DALI_FSM_REPL_LOCK_ACC,
	DALI_FSM_REPL_SEARCH,
	DALI_FSM_REPL_FIND,
	DALI_FSM_REPL_RESET,
	DALI_FSM_REPL_INITIALIZE,
	DALI_FSM_REPL_RANDOMIZE,
	DALI_FSM_REPL_INIT_BISECTION,
	DALI_FSM_REPL_COMPARE,
	DALI_FSM_REPL_RESPONSE_BALLAST_COMPARE,
	DALI_FSM_REPL_QUERY_SHORT,
	DALI_FSM_REPL_RESPONSE_QUERY_SHORT,
	DALI_FSM_REPL_PROGRAM_SHORT_ADDR,
	DALI_FSM_REPL_VERIFY_SHORT_ADDR,
	DALI_FSM_REPL_RESPONSE_BALLAST_SHORT_ADDR,
	DALI_FSM_REPL_WITHDRAW,
	DALI_FSM_REPL_COMPAREW,
	DALI_FSM_REPL_RESPONSE_BALLAST_WITHDRAW,
	DALI_FSM_REPL_TERMINATE
} enDaliFsmReplState_t;

typedef enum {
	
	DALI_FSM_TEST_BALLAST_INIT,
	DALI_FSM_TEST_BALLAST_RECALL_MAX_LEVEL,
	DALI_FSM_TEST_BALLAST_STANDBY_RECALL_MAX_LEVEL,
	DALI_FSM_TEST_BALLAST_RECALL_MIN_LEVEL,
	DALI_FSM_TEST_BALLAST_STANDBY_RECALL_MIN_LEVEL,
	DALI_FSM_TEST_BALLAST_STOP,
	DALI_FSM_TEST_BALLAST_TIMEOUT
	
}enDaliFsmTestBallast_t;

typedef enum {
	DALI_FSM_TEST_BALLAST_GROUPE_INIT,
	DALI_FSM_TEST_BALLAST_GROUPE_RECALL_MAX_LEVEL,
	DALI_FSM_TEST_BALLAST_GROUPE_STANDBY_RECALL_MAX_LEVEL,
	DALI_FSM_TEST_BALLAST_GROUPE_RECALL_MIN_LEVEL,
	DALI_FSM_TEST_BALLAST_GROUPE_STANDBY_RECALL_MIN_LEVEL,
	DALI_FSM_TEST_BALLAST_GROUPE_STOP,
	DALI_FSM_TEST_BALLAST_GROUPE_TIMEOUT
	
}enDaliFsmTestBallastGroupe_t;



typedef enum  {
	DALI_TYPE_RSP_BALLAST,
	DALI_TYPE_RSP_ACC,
	DALI_TYPE_RSP_DALI_2
} enDaliGenericRxFrame_t;

typedef struct
{
	uint8_t ucDtr1;
	uint8_t ucDtr;
	uint8_t ucAddrDest;
	uint8_t ucLsb;
	uint8_t ucMsb;
}stcDaliAccRxFrame_t;

typedef struct
{
	uint8_t ucCodeTypeRsp;/*Rsp ballast / Rsp acc / Rsp DALI2*/
	stcDaliAccRxFrame_t stcDaliAccRxFrame;
}stcDaliGenericRxFrame_t;

typedef enum  {
    DALI_RX_DISP_STATE_INIT,
	DALI_RX_DISP_STATE_WAIT_FIFO,
	DALI_RX_DISP_STATE_PARSE_DATA
} enDaliRxDispatcherState_t;

typedef enum  {
    DALI_COMPARE_ADDR_AUTO_IN_PROGRESS,
	DALI_COMPARE_ADDR_AUTO_RX_NOK,
	DALI_COMPARE_ADDR_AUTO_RX_OK
} enDaliCompareAddrAuto_t;

typedef enum  {
    DALI_COMPARE_ADDR_AUTO_STATE_RX_OK,
	DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK
} enDaliCompareAddrAutoState_t;

void DaliFsmRx(void);
void DecoderFrameDaliRx(void);
void CallBackTimDaliBitStop(void);
void CallBackTimDaliInterTram(void);
void InitDali(void);
void GpioDaliInit(void);
void timDaliCptBetweenFrontInit(void);
VOID_FUNCTION_PIN_RX_DALI_INTERRUPT_VOID;
/*
 *
 * Tick must be in ms
 *
 * */
void TimerDali(uint8_t ucTimerName, uint8_t ucState, uint16_t usTimeCount);
/*
 *
 * Tick must be in ms
 *
 * */
/*
 *
 * TimerDaliScheduler must be call in loop while(1)
 *
 * */
#if (USE_SCHEDULER)
void TimerDaliScheduler(en_change_behavior_fnt_t en_change_behavior_fnt);/*!!!!*/
#else
void TimerDaliScheduler(void);
#endif
/*
 *
 * TimerDaliScheduler must be call in loop while(1)
 *
 *
 * */
uint32_t OffsetOptoDali(uint8_t ucPreviousFrontTmp, uint8_t ucActualFrontTmp, uint32_t uiValueCptTimer);
void SetAddrAutoDali(uint8_t ucTmp);
uint8_t GetBlocageTxDali(void);
void SetPercentageTolerance(uint8_t ucTmp);
void SetCorrectionOpto(uint8_t ucTmp);
uint16_t GetCorrectionOptoCalibration(void);
void SetRxCalibrationProcessing(uint8_t ucTmp);
//void DaliFsmTxTask(en_change_behavior_fnt_t en_change_behavior_fnt);
void ParseDataDaliTx(void);
void CallBackTimerDaliTx(void);
void CallBackTimInterPacketTx(void);
void CallBackTimInterFrameTx(void);
void timDaliFrontTransitionTxInit(void);
int8_t SetFrameTx(uint8_t ucMsb, uint8_t ucLsb, uint8_t ucCmd, uint8_t ucDtr, uint8_t ucDtr1, uint8_t ucAdresse);
int8_t SimpleSetFrameTx(uint8_t ucCodeDali);
//void DaliFsmConfigBalast(en_change_behavior_fnt_t en_change_behavior_fnt);
//void DaliFsmRxDispatcherTask(en_change_behavior_fnt_t en_change_behavior_fnt);
void SetAddrAutoDaliRuntime(void);

void SetRemplaceDaliRuntime(void);
//void SetAddGroupeDaliRuntime(stcDaliCnfBalAddDelGrp_t *stcDaliCnfBalAddDelGrp_pt);
//void SetDelGroupeDaliRuntime(stcDaliCnfBalAddDelGrp_t *stcDaliCnfBalAddDelGrp_pt);
void SetQueryDaliRuntime(void);
//void SetTestDaliBallastRuntime(stcDaliCnfTestBallast_t *stcDaliCnfTestBallast_pt);
//void SetTestDaliGroupeRuntime(stcDaliCnfTestGroupe_t *stcDaliCnfTestGroupe_pt);
uint16_t GetConfigDali(uint8_t ucSelect);
//void SetConfigDali(stcSystemConfig_t *stcSystemConfig_pt);
//void SetDaliGateway(stcRuntimeDaliGateway_t *stcRuntimeDaliGateway_pt);
//void DaliFsmWatchLine(en_change_behavior_fnt_t en_change_behavior_fnt);
uint8_t GetConfigDaliBallast(void);
void DaliConfigRemoteControllerBallastTest(stcDaliGenericRxFrame_t *stcDaliGenericRxFrame_pt);
void TEST(void);

#endif /* __DALI_H */
