/**
 * @file fifos.h
 * @author R&D Team (arcom@groupe-arcom.com)
 * @brief Simple fifo implemetation
 * @version 1.0
 * @date 24-01-2020
 * 
 * @copyright Copyright Arcom 2020
 * 
 */
#ifndef __FIFOS_H
#define __FIFOS_H

#include <stdint.h>

#include "DALI.h"



/* Exported constants --------------------------------------------------------*/
#define MAX_FRAMES_IN_FIFO_SPI_TX     10
#define MAX_FRAMES_IN_FIFO_SPI_RX     10

#define MAX_FRAMES_IN_FIFO_DALI_TX    100
#define MAX_FRAMES_IN_FIFO_DALI_RX    20

#define MAX_FRAMES_IN_FIFO_TIC_LABEL_DATA 	20


#define SIZE_FIFO_DALI_TX              MAX_FRAMES_IN_FIFO_DALI_TX*SIZE_DALI_TX_FRAME
#define SIZE_FIFO_DALI_RX              MAX_FRAMES_IN_FIFO_DALI_RX*SIZE_DALI_RX_FRAME



/*FIFO's name*/
typedef enum  {
	DALI_TX_FIFO,
	DALI_RX_FIFO,
	SIZE_FIFO
} enFifoNames_t;
/* Exported functions ------------------------------------------------------- */
void InitFIFOs(void);
int8_t Push(uint8_t *data, uint16_t size, uint8_t fifo);
int8_t Pop(uint8_t *data, uint16_t size, uint8_t fifo);
int8_t ReadPop(uint8_t *data, uint16_t size, uint8_t fifo);
uint16_t GetFifoNbBytes(uint8_t fifo);
int8_t PushByte(uint8_t byte, uint8_t fifo);

void ClearFIFO(uint8_t fifo);


#endif
