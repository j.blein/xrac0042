/**
 * @file tick.h
 * @author R&D Team (arcom@groupe-arcom.com)
 * @brief 
 * @version 1.0
 * @date 06-12-2019
 * 
 * @copyright Copyright Arcom 2019
 * 
 */
#ifndef TICK_H /** Prevent multiple inclusions **/
#define TICK_H

//#include <stdint.h>
//#include "stm32g0xx_hal.h"
#include "stm32f0xx_hal.h"

/* Exported types---------------------------------------------*/
typedef uint32_t TICK;

#define  HAL_TICK_FREQ_10HZ         100U
#define  HAL_TICK_FREQ_100HZ        10U
#define  HAL_TICK_FREQ_1KHZ         1U
#define  HAL_TICK_FREQ_DEFAULT      HAL_TICK_FREQ_1KHZ

/* Exported constants---------------------------------------------*/
#define SYSTICK_FREQ            (1000/HAL_TICK_FREQ_1KHZ)
#define TICKS_PER_SECOND		((uint32_t)SYSTICK_FREQ)
          
#define TICK_SECOND				((uint32_t)TICKS_PER_SECOND)       /*Represents one second in Ticks*/
#define TICK_MINUTE				((uint32_t)TICKS_PER_SECOND*60)    /*Represents one minute in Ticks*/
#define TICK_HOUR				((uint32_t)TICKS_PER_SECOND*3600)  /*Represents one hour in Ticks*/
/* Exported macro ------------------------------------------------------------*/
#define TICK_GET_DIFF(a, b)       ((a)-(b))

/** Exported functions---------------------------------------------*/
uint32_t    tickGet(void);
void        tickSleep(uint32_t timeout);

#endif /*TEMPLATE_H*/
