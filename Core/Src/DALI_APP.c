
#include "DALI_APP.h"
#include "DALI.h"
#include <string.h>
#include "config.h"


static stcDaliCnfBalAddDelGrp_t stcDaliCnfBalAddDelGrp = {0};
static stcDaliCnfBalIdInverGrp_t stcDaliCnfBalIdInverGrp = {0};
static stcDaliCnfTestBallast_t stcDaliCnfTestBallast = {0};
static stcDaliCnfTestGroupe_t stcDaliCnfTestGroupe = {0};


/******************************************************************************/
/*!\brief DaliFsmInvTask
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void DaliFsmInvTask(void)
{

#if(0) //JBL
	static uint8_t ucPhaseInvert = 0;
	static uint64_t ulAddrBalastBusy = 0;
	static uint8_t ucReplaceAdd = 0;
	static uint8_t ucAddrBallastLibre = 0;
	static uint64_t ulAddReplace = 0;


    switch(enDaliFsmInvState)
    {
    	case DALI_FSM_INV_INIT :
        	ucPhaseInvert=0;
        	ulAddrBalastBusy=0;
        	ucReplaceAdd=0;
        	ucAddrBallastLibre=0;
        	ulAddReplace = 0;
    		enDaliFsmInvState=DALI_FSM_INV_OFF;
    	break;
    	case DALI_FSM_INV_OFF :
         	SimpleSetFrameTx(CMD_DALI_SET_OFF);
    		enDaliFsmInvState=DALI_FSM_INV_LOCK_ACC;
    	break;
    	case DALI_FSM_INV_LOCK_ACC :
        	SetFrameTx(0xC0+(ADDR_DALI_MASTER & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,63);
    		enDaliFsmInvState=DALI_FSM_INV_SEARCH;
    	break;
    	case DALI_FSM_INV_SEARCH :
    		if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
    		{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,145,(ucAddrBallastLibre<<1)+1,1,0);
				enDaliFsmInvState=DALI_FSM_INV_FIND;
    		}
    	break;
    	case DALI_FSM_INV_FIND :
        	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
        	{
        		if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
        		{
        			ulAddrBalastBusy+=(1<<ucAddrBallastLibre);
        		}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{
					if(ucReplaceAdd==0)
					{
						ucReplaceAdd=(ucAddrBallastLibre<<1)+1;
					}
        		}
        		ucAddrBallastLibre++;
				if(ucAddrBallastLibre == 33)
				{
					enDaliFsmInvState = DALI_FSM_INV_STEP1;
				}
				else
				{
					enDaliFsmInvState = DALI_FSM_INV_SEARCH;
				}
        	}
    	break;
    	case DALI_FSM_INV_STEP1 :
    		if((((ulAddrBalastBusy>>stcDaliCnfBalIdInverGrp.ucID1)&0x01)==0)&&(((ulAddrBalastBusy>>stcDaliCnfBalIdInverGrp.ucID2)&0x01)==0))
    		{
    			enDaliFsmInvState = DALI_FSM_INV_TERMINATE;
    			break;
    		}
    		else if(((ulAddrBalastBusy>>stcDaliCnfBalIdInverGrp.ucID1)&0x01)==0)
    		{
    			SetFrameTx(0,0,(stcDaliCnfBalIdInverGrp.ucID1<<1)+1,CMD_DALI_STORE_IN_DTR,1,0);
    			SetFrameTx(0,0,CMD_DALI_STORE_DTR_AS_SHORT_ADR,(stcDaliCnfBalIdInverGrp.ucID2<<1)+1,1,0);
    			ulAddReplace=(stcDaliCnfBalIdInverGrp.ucID1<<1)+1;
    			ucPhaseInvert=0x20;
   			}
    		else if(((ulAddrBalastBusy>>stcDaliCnfBalIdInverGrp.ucID2)&0x01)==0)
    		{
    			SetFrameTx(0,0,(stcDaliCnfBalIdInverGrp.ucID2<<1)+1,CMD_DALI_STORE_IN_DTR,1,0);
    			SetFrameTx(0,0,CMD_DALI_STORE_DTR_AS_SHORT_ADR,(stcDaliCnfBalIdInverGrp.ucID1<<1)+1,1,0);
    			ulAddReplace=(stcDaliCnfBalIdInverGrp.ucID2<<1)+1;
    			ucPhaseInvert=0x20;
    		}
    		else
    		{
    			SetFrameTx(0,0,ucReplaceAdd,CMD_DALI_STORE_IN_DTR,1,0);
    			SetFrameTx(0,0,CMD_DALI_STORE_DTR_AS_SHORT_ADR,(stcDaliCnfBalIdInverGrp.ucID1<<1)+1,1,0);
    			ulAddReplace=ucReplaceAdd;
    		}
    		enDaliFsmInvState=DALI_FSM_INV_QUERY1;
    	break;
    	case DALI_FSM_INV_QUERY1 :
    		if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
    		{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
        		SetFrameTx(0,1,145,ulAddReplace,1,0);
        		enDaliFsmInvState=DALI_FSM_INV_STEP2;
    		}
    	break;
    	case DALI_FSM_INV_STEP2 :
        	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
        	{
        		if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
        		{
        			if(ucPhaseInvert==0x20)
        			{
        				enDaliFsmInvState = DALI_FSM_INV_TERMINATE;
        			}
        			else
        			{
        				SetFrameTx(0,0,(stcDaliCnfBalIdInverGrp.ucID1<<1)+1,CMD_DALI_STORE_IN_DTR,1,0);
        				SetFrameTx(0,0,CMD_DALI_STORE_DTR_AS_SHORT_ADR,(stcDaliCnfBalIdInverGrp.ucID2<<1)+1,1,0);
						ulAddReplace=(stcDaliCnfBalIdInverGrp.ucID1<<1)+1;
						enDaliFsmInvState=DALI_FSM_INV_QUERY2;
					}
        		}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{
        			enDaliFsmInvState = DALI_FSM_INV_TERMINATE;
        		}
        	}
    	break;
    	case DALI_FSM_INV_QUERY2 :
    		if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
    		{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,145,ulAddReplace,1,0);
        		enDaliFsmInvState=DALI_FSM_INV_STEP3;
    		}

    	break;
    	case DALI_FSM_INV_STEP3 :
    		if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
			{
				if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
				{
					SetFrameTx(0,0,(stcDaliCnfBalIdInverGrp.ucID2<<1)+1,CMD_DALI_STORE_IN_DTR,1,0);
					SetFrameTx(0,0,CMD_DALI_STORE_DTR_AS_SHORT_ADR,ucReplaceAdd,1,0);
					ulAddReplace=(stcDaliCnfBalIdInverGrp.ucID2<<1)+1;
					enDaliFsmInvState=DALI_FSM_INV_QUERY3;
				}
				else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
				{
					enDaliFsmInvState=DALI_FSM_INV_TERMINATE;
				}
			}
    	break;
    	case DALI_FSM_INV_QUERY3 :
    		if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
    		{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,145,ulAddReplace,1,0);
        		enDaliFsmInvState=DALI_FSM_INV_STEP4;
    		}

    	break;
    	case DALI_FSM_INV_STEP4 :
    		if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
			{
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
				enDaliFsmInvState=DALI_FSM_INV_TERMINATE;
			}
    	break;
    	case DALI_FSM_INV_TERMINATE :
    		SetFrameTx((ADDR_DALI_MASTER & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,63);
    		SetFrameTx(0,0,CMD_DALI_SET_OFF,0xFF,1,0);
			ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_STANDBY;
			enDaliFsmInvState = DALI_FSM_INV_INIT;
			ucAddrAutoDali=USER_FALSE;
    	break;
		default:
			enDaliFsmInvState = DALI_FSM_INV_INIT;
		break;
    }
#endif
}

/******************************************************************************/
/*!\brief DaliFsmAddDelGTask
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void DaliFsmAddDelGTask(void)
{
#if(0) //JBL
	uint8_t ucI = 0;

    switch(enDaliFsmAddDelGState)
    {

		case DALI_FSM_ADD_DEL_G_INIT :
			enDaliFsmAddDelGState=DALI_FSM_ADD_DEL_G_OFF;
		break;
		case DALI_FSM_ADD_DEL_G_OFF :
			SimpleSetFrameTx(CMD_DALI_SET_OFF);
			enDaliFsmAddDelGState=DALI_FSM_ADD_DEL_G_LOCK_ACC;
		break;
		case DALI_FSM_ADD_DEL_G_LOCK_ACC :
			SimpleSetFrameTx(CMD_DALI_BLOCAGE);
			if(ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_ADD_GRP_TYPE)
			{
				enDaliFsmAddDelGState=DALI_FSM_ADD_DEL_G_ADD;
			}
			else if(ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_DEL_ADDR_TYPE)
			{
				enDaliFsmAddDelGState=DALI_FSM_ADD_DEL_G_DEL;
			}
			else
			{
				SimpleSetFrameTx(CMD_DALI_RECALL_MAX_LEVEL);
				enDaliFsmAddDelGState= DALI_FSM_ADD_DEL_G_TERMINATE;
			}
		break;
		case DALI_FSM_ADD_DEL_G_ADD :
			for(ucI=0;ucI<NB_MAX_GROUPE_BALLAST_CONFIG;ucI++)
			{
				if(stcDaliCnfBalAddDelGrp.ucIDs_a[ucI] != 0)
				{
					SetFrameTx(0,0,CMD_DALI_ADD_TO_GROUP0+stcDaliCnfBalAddDelGrp.ucGrp,(stcDaliCnfBalAddDelGrp.ucIDs_a[ucI]<<1)+1,1,0);
				}
			}
			SimpleSetFrameTx(CMD_DALI_RECALL_MAX_LEVEL);
			enDaliFsmAddDelGState= DALI_FSM_ADD_DEL_G_TERMINATE;
		break;
		case DALI_FSM_ADD_DEL_G_DEL :
			for(ucI=0;ucI<NB_MAX_GROUPE_BALLAST_CONFIG;ucI++)
			{
				if(stcDaliCnfBalAddDelGrp.ucIDs_a[ucI] != 0)
				{
					SetFrameTx(0,0,CMD_DALI_REMOVE_FROM_GROUP0+stcDaliCnfBalAddDelGrp.ucGrp,(stcDaliCnfBalAddDelGrp.ucIDs_a[ucI]<<1)+1,1,0);
				}
			}
			SimpleSetFrameTx(CMD_DALI_RECALL_MAX_LEVEL);
			enDaliFsmAddDelGState= DALI_FSM_ADD_DEL_G_TERMINATE;
		break;
    	case DALI_FSM_ADD_DEL_G_TERMINATE :
    		SetFrameTx((ADDR_DALI_MASTER & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,63);
    		SimpleSetFrameTx(CMD_DALI_SET_OFF);
			ucFlagWaitStartAddrAuto=DALI_BALLAST_CONF_STANDBY;
			enDaliFsmAddDelGState= DALI_FSM_ADD_DEL_G_INIT;
		break;
		default:
			enDaliFsmAddDelGState = DALI_FSM_ADD_DEL_G_INIT;
		break;
    }
#endif
}

/******************************************************************************/
/*!\brief DaliFsmTestBallastTask
 * \author Jérémy BLEIN
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void DaliFsmTestBallastTask(void)
{
#if(0) //JBL
	static uint8_t ucStateBallast_s = 0xFF;
	static TICK uiLastTick_s;
	static TICK uiTickTimeout_s;
    TICK uiCurrentTick_s;

    /* New state */
    if(ucStateBallast_s != stcDaliCnfTestBallast.ucStateTestBallast)
    {
    	if(stcDaliCnfTestBallast.ucStateTestBallast == TEST_BALLAST_START)
    	{
    		enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_INIT;
    		ucStateBallast_s = stcDaliCnfTestBallast.ucStateTestBallast;
    	}
    	else if(stcDaliCnfTestBallast.ucStateTestBallast == TEST_BALLAST_STOP)
    	{
    		enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_STOP;
    		ucStateBallast_s = stcDaliCnfTestBallast.ucStateTestBallast;
    	}
    }

	switch(enDaliFsmTestBallast)
	{
		case(DALI_FSM_TEST_BALLAST_INIT):
		{
			uiTickTimeout_s = tickGet();
			SimpleSetFrameTx(CMD_DALI_SET_OFF);
			enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_RECALL_MAX_LEVEL;
		}break;

		case(DALI_FSM_TEST_BALLAST_RECALL_MAX_LEVEL):
		{
			uiLastTick_s = tickGet();
			SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,stcDaliCnfTestBallast.ucBallastAddress,1,0);
			enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_STANDBY_RECALL_MAX_LEVEL;
		}break;

		case(DALI_FSM_TEST_BALLAST_STANDBY_RECALL_MAX_LEVEL):
		{
	    	uiCurrentTick_s = tickGet();

	    	if(uiLastTick_s > uiCurrentTick_s)
	    	{/*protection*/
	    		uiLastTick_s = uiCurrentTick_s;
	    	}

	    	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= 2*TICK_SECOND)
	    	{
	    		enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_RECALL_MIN_LEVEL;
	    	}
		}break;

		case(DALI_FSM_TEST_BALLAST_RECALL_MIN_LEVEL):
		{
			uiLastTick_s = tickGet();
			SetFrameTx(0,0,CMD_DALI_RECALL_MIN_LEVEL,stcDaliCnfTestBallast.ucBallastAddress,1,0);
			enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_STANDBY_RECALL_MIN_LEVEL;
		}break;

		case(DALI_FSM_TEST_BALLAST_STANDBY_RECALL_MIN_LEVEL):
		{
	    	uiCurrentTick_s = tickGet();

	    	if(uiLastTick_s > uiCurrentTick_s)
	    	{/*protection*/
	    		uiLastTick_s = uiCurrentTick_s;
	    	}

	    	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= 2*TICK_SECOND)
	    	{
	    		enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_TIMEOUT;
	    	}
		}break;

		case(DALI_FSM_TEST_BALLAST_TIMEOUT):
		{
	    	uiCurrentTick_s = tickGet();

	    	if(TICK_GET_DIFF(uiCurrentTick_s,uiTickTimeout_s) >= 60*TICK_SECOND)
	    	{
				enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_STOP;
	    	}
	    	else
	    	{
	    		enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_RECALL_MAX_LEVEL;
	    	}
		}break;

		case(DALI_FSM_TEST_BALLAST_STOP):
		{
			SimpleSetFrameTx(CMD_DALI_SET_OFF);
			ucFlagWaitStartAddrAuto=DALI_BALLAST_CONF_STANDBY;
			enDaliFsmTestBallast= DALI_FSM_TEST_BALLAST_INIT;
			ucStateBallast_s = 0xFF;
		}break;
	}
#endif
}

/******************************************************************************/
/*!\brief DaliFsmTestBallastGroupeTask
 * \author Jérémy BLEIN
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void DaliFsmTestGroupeBallastTask(void)
{
#if(0) //JBL
	static uint8_t ucStateBallast_s = 0xFF;
	static TICK uiLastTick_s;
	static TICK uiTickTimeout_s;
    TICK uiCurrentTick_s;

    /* New state */
    if(ucStateBallast_s != stcDaliCnfTestGroupe.ucStateTestBallast)
    {
    	if(stcDaliCnfTestGroupe.ucStateTestBallast == TEST_BALLAST_START)
    	{
    		enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_INIT;
    		ucStateBallast_s = stcDaliCnfTestGroupe.ucStateTestBallast;
    	}
    	else if(stcDaliCnfTestGroupe.ucStateTestBallast == TEST_BALLAST_STOP)
    	{
    		enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_STOP;
    		ucStateBallast_s = stcDaliCnfTestGroupe.ucStateTestBallast;
    	}
    }

	switch(enDaliFsmTestBallastGroupe)
	{
		case(DALI_FSM_TEST_BALLAST_GROUPE_INIT):
		{
			uiTickTimeout_s = tickGet();
			SimpleSetFrameTx(CMD_DALI_SET_OFF);
			enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_RECALL_MAX_LEVEL;
		}break;

		case(DALI_FSM_TEST_BALLAST_GROUPE_RECALL_MAX_LEVEL):
		{
			uiLastTick_s = tickGet();
			SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,stcDaliCnfTestGroupe.ucGroupeAddress,1,0);
			enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_STANDBY_RECALL_MAX_LEVEL;
		}break;

		case(DALI_FSM_TEST_BALLAST_GROUPE_STANDBY_RECALL_MAX_LEVEL):
		{
	    	uiCurrentTick_s = tickGet();

	    	if(uiLastTick_s > uiCurrentTick_s)
	    	{/*protection*/
	    		uiLastTick_s = uiCurrentTick_s;
	    	}

	    	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= 2*TICK_SECOND)
	    	{
	    		enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_RECALL_MIN_LEVEL;
	    	}
		}break;

		case(DALI_FSM_TEST_BALLAST_GROUPE_RECALL_MIN_LEVEL):
		{
			uiLastTick_s = tickGet();
			SetFrameTx(0,0,CMD_DALI_RECALL_MIN_LEVEL,stcDaliCnfTestGroupe.ucGroupeAddress,1,0);
			enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_STANDBY_RECALL_MIN_LEVEL;
		}break;

		case(DALI_FSM_TEST_BALLAST_GROUPE_STANDBY_RECALL_MIN_LEVEL):
		{
	    	uiCurrentTick_s = tickGet();

	    	if(uiLastTick_s > uiCurrentTick_s)
	    	{/*protection*/
	    		uiLastTick_s = uiCurrentTick_s;
	    	}

	    	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= 2*TICK_SECOND)
	    	{
	    		enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_TIMEOUT;
	    	}
		}break;

		case(DALI_FSM_TEST_BALLAST_GROUPE_TIMEOUT):
		{
	    	uiCurrentTick_s = tickGet();

	    	if(TICK_GET_DIFF(uiCurrentTick_s,uiTickTimeout_s) >= 60*TICK_SECOND)
	    	{
	    		enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_STOP;
	    	}
	    	else
	    	{
	    		enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_RECALL_MAX_LEVEL;
	    	}
		}break;

		case(DALI_FSM_TEST_BALLAST_GROUPE_STOP):
		{
			SimpleSetFrameTx(CMD_DALI_SET_OFF);
			ucFlagWaitStartAddrAuto=DALI_BALLAST_CONF_STANDBY;
			enDaliFsmTestBallastGroupe= DALI_FSM_TEST_BALLAST_GROUPE_INIT;
			ucStateBallast_s = 0xFF;
		}break;
	}
#endif
}

/******************************************************************************/
/*!\brief SetAddrAutoDaliRuntime
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetAddrAutoDaliRuntime(void)
{
#if(0) //JBL
	if( (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_STANDBY)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_GROUPE_TYPE)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_BALLAST_TYPE) )
	{
		ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_ADDR_AUTO_TYPE;
	}
#endif
}

/******************************************************************************/
/*!\brief SetInvertionDaliRuntime
 * \author Jimmy ZANOTTO
 *
 * \param       stcDaliCnfBalIdInverGrp_t *stcDaliCnfBalIdInverGrp_pt
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetInvertionDaliRuntime(stcDaliCnfBalIdInverGrp_t *stcDaliCnfBalIdInverGrp_pt)
{
#if(0) //JBL
	if( (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_STANDBY)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_GROUPE_TYPE)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_BALLAST_TYPE) )
	{
		ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_INVERSION_TYPE;
		memcpyST((uint8_t*)&stcDaliCnfBalIdInverGrp,(const uint8_t *)stcDaliCnfBalIdInverGrp_pt,SIZE_OF_CNF_BAL_INV_ID_GRP);
	}
#endif
}

/******************************************************************************/
/*!\brief SetRemplaceDaliRuntime
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetRemplaceDaliRuntime(void)
{
#if(0) //JBL
	if( (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_STANDBY)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_GROUPE_TYPE)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_BALLAST_TYPE) )
	{
		ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_REPLACE_TYPE;
	}
#endif
}

/******************************************************************************/
/*!\brief SetAddGroupeDaliRuntime
 * \author Jimmy ZANOTTO
 *
 * \param       stcDaliCnfBalAddDelGrp_t *stcDaliCnfBalAddDelGrp_pt
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetAddGroupeDaliRuntime(stcDaliCnfBalAddDelGrp_t *stcDaliCnfBalAddDelGrp_pt)
{
#if(0) //JBL
	if( (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_STANDBY)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_GROUPE_TYPE)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_BALLAST_TYPE) )
	{
		ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_ADD_GRP_TYPE;
		memcpyST((uint8_t*)&stcDaliCnfBalAddDelGrp,(const uint8_t *)stcDaliCnfBalAddDelGrp_pt,SIZE_OF_CNF_BAL_ADD_DEL_GRP);
	}
#endif
}

/******************************************************************************/
/*!\brief SetDelGroupeDaliRuntime
 * \author Jimmy ZANOTTO
 *
 * \param       stcDaliCnfBalAddDelGrp_t *stcDaliCnfBalAddDelGrp_pt
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetDelGroupeDaliRuntime(stcDaliCnfBalAddDelGrp_t *stcDaliCnfBalAddDelGrp_pt)
{
#if(0) //JBL
	if( (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_STANDBY)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_GROUPE_TYPE)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_BALLAST_TYPE) )
	{
		ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_DEL_ADDR_TYPE;
		memcpyST((uint8_t*)&stcDaliCnfBalAddDelGrp,(const uint8_t *)stcDaliCnfBalAddDelGrp_pt,SIZE_OF_CNF_BAL_ADD_DEL_GRP);
	}
#endif
}

/******************************************************************************/
/*!\brief DaliFsmConfigBalast
 * \author JZA + RFR + AME
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
#ifdef USE_SCHEDULER
void DaliFsmConfigBalast(en_change_behavior_fnt_t en_change_behavior_fnt)
#else
void DaliFsmConfigBalast(void)
#endif
{
#if(0) //JBL
    static TICK uiLastTick_s = 0;
    TICK uiCurrentTick_s;
    uint16_t usBytesInFifo = 0;

	uiCurrentTick_s = tickGet();
	if(uiLastTick_s == 0)
	{
		uiLastTick_s = uiCurrentTick_s;
	}

	if(uiLastTick_s > uiCurrentTick_s)
	{/*protection*/
		uiLastTick_s = uiCurrentTick_s;
	}


	if(ucLineReady_g == DALI_LINE_OK)
	{
	    switch(ucFlagWaitStartAddrAuto)
	    {
	        case DALI_BALLAST_CONF_STANDBY :
	        	uiLastTick_s = uiCurrentTick_s;

	        	if(uiLastTick_s > uiCurrentTick_s)
	        	{/*protection*/
	        		uiLastTick_s = uiCurrentTick_s;
	        	}
	        break;
	        case DALI_BALLAST_CONF_ADDR_AUTO_TYPE :
	        	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= TIME_ACTIVATION_RELAY_230)
	        	{
		        	DaliFsmAdrAutoTask();
	        	}
	        	else
	        	{
	        		ManagementDaliLight230(OUTPUT_Y230_ACTIF);
	        	}
	        break;
	        case DALI_BALLAST_CONF_INVERSION_TYPE :
	        	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= TIME_ACTIVATION_RELAY_230)
	        	{
		        	DaliFsmInvTask();
	        	}
	        	else
	        	{
	        		ManagementDaliLight230(OUTPUT_Y230_ACTIF);
	        	}
	        break;
	        case DALI_BALLAST_CONF_REPLACE_TYPE :
	        	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= TIME_ACTIVATION_RELAY_230)
	        	{
		        	DaliFsmReplTask();
	        	}
	        	else
	        	{
	        		ManagementDaliLight230(OUTPUT_Y230_ACTIF);
	        	}
	        break;
	        case DALI_BALLAST_CONF_ADD_GRP_TYPE :
	        	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= TIME_ACTIVATION_RELAY_230)
	        	{
		        	DaliFsmAddDelGTask();
	        	}
	        	else
	        	{
	        		ManagementDaliLight230(OUTPUT_Y230_ACTIF);
	        	}
	        break;
	        case DALI_BALLAST_CONF_DEL_ADDR_TYPE :
	        	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= TIME_ACTIVATION_RELAY_230)
	        	{
		        	DaliFsmAddDelGTask();
	        	}
	        	else
	        	{
	        		ManagementDaliLight230(OUTPUT_Y230_ACTIF);
	        	}
	        break;
	        case DALI_BALLAST_CONF_QUERY_TYPE :
	        	ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_STANDBY;
	        break;
			case DALI_BALLAST_CONF_TEST_BALLAST_TYPE :
	        	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= TIME_ACTIVATION_RELAY_230)
	        	{
					DaliFsmTestBallastTask();
	        	}
	        	else
	        	{
	        		ManagementDaliLight230(OUTPUT_Y230_ACTIF);
	        	}

			break;
			case DALI_BALLAST_CONF_TEST_GROUPE_TYPE :
	        	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= TIME_ACTIVATION_RELAY_230)
	        	{
					DaliFsmTestGroupeBallastTask();
	        	}
	        	else
	        	{
	        		ManagementDaliLight230(OUTPUT_Y230_ACTIF);
	        	}

			break;
	        default:
	        	ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_STANDBY;


	        break;
	    }
	}
#endif
}


void DaliConfigRemoteControllerBallastTest(stcDaliGenericRxFrame_t *stcDaliGenericRxFrame_pt)
{
#if(0) //JBL
	uint8_t ucAddrAcc = 0x00;
	uint8_t ucMsbAcc = 0x00;
	stcDaliTxFrame_t stcDaliTxFrameTmp = {0};

	ucAddrAcc = MASK_ADDR_DALI & stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucMsb;
	ucMsbAcc = ( (MASK_MSB_DALI & stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucMsb) >> 6);

	switch(stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucDtr1)
	{
		case(CMD_DALI_VERSION_ACC):
		{
			stcDaliTxFrameTmp.ucCmd = CMD_DALI_ENABLE_WRITE_MEMORY;
			stcDaliTxFrameTmp.ucDtr1 = CMD_DALI_ETAT_TEST_DALI;
			stcDaliTxFrameTmp.ucDtr = 0;
			stcDaliTxFrameTmp.ucLsb = 0x1E;
			stcDaliTxFrameTmp.ucMsb = ADDR_DALI_MASTER;
			stcDaliTxFrameTmp.ucAdresse = ucAddrAcc;
			Push((uint8_t*)&stcDaliTxFrameTmp,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);
		}break;

		case(CMD_DALI_CONFIG_BALLAST):
		{
			if(ucMsbAcc == 0x00)
			{
				if(stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb == 0x0B)
				{
					SetAddrAutoDaliRuntime();
				}
			}
		}break;

		case(CMD_DALI_CONFIG_BALLAST_2):
		{
			if(ucMsbAcc == 0x00)
			{
				stcDaliCnfBalIdInverGrp_t stcDaliCnfBalIdInverGrpTemp = {0};

				stcDaliCnfBalIdInverGrpTemp.ucID1 = stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb + 1;
				stcDaliCnfBalIdInverGrpTemp.ucID2 = stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucDtr + 1;
				SetInvertionDaliRuntime(&stcDaliCnfBalIdInverGrpTemp);
			}
			else if( ucMsbAcc == 0x01)
			{
				SetRemplaceDaliRuntime();
			}
		}break;

		case(CMD_DALI_TEST_BALLAST_2):
		{
			if( (ucMsbAcc == 0x00) || (ucMsbAcc == 0x01) )
			{
				stcDaliCnfTestBallast_t stcDaliCnfTestBallastTemp;

				stcDaliCnfTestBallastTemp.ucStateTestBallast = ucMsbAcc;

				if(stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb != 0xFF)
				{
					stcDaliCnfTestBallastTemp.ucBallastAddress = (stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb << 1)+1;
				}
				else
				{
					stcDaliCnfTestBallastTemp.ucBallastAddress = stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb;
				}
				SetTestDaliBallastRuntime(&stcDaliCnfTestBallastTemp);
			}
		}break;

		case(CMD_AJOUT_BALLAST_GROUPE):
		case(CMD_SUPP_BALLAST_GROUPE):
		{
			stcDaliCnfBalAddDelGrp_t stcDaliCnfBalAddDelGrpTemp = {0};
			uint8_t ucAdresseBallastOffset = 0;
			unucDataToBit_t unDataToBit = {0};

			if(stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb <= 16)
			{
				stcDaliCnfBalAddDelGrpTemp.ucGrp = stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb;
				unDataToBit.ucDataToBit = stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucDtr;

				switch(ucMsbAcc)
				{
					case(0):
					{
						ucAdresseBallastOffset = 0;
					}break;

					case(1):
					{
						ucAdresseBallastOffset = 8;
					}break;

					case(2):
					{
						ucAdresseBallastOffset = 16;
					}break;

					case(3):
					{
						ucAdresseBallastOffset = 24;
					}break;
				}

				if(unDataToBit.ucBit0 == 1)
				{
					stcDaliCnfBalAddDelGrpTemp.ucIDs_a[0] = ucAdresseBallastOffset + 1;
				}
				if(unDataToBit.ucBit1 == 1)
				{
					stcDaliCnfBalAddDelGrpTemp.ucIDs_a[1] = ucAdresseBallastOffset + 2;
				}
				if(unDataToBit.ucBit2 == 1)
				{
					stcDaliCnfBalAddDelGrpTemp.ucIDs_a[2] = ucAdresseBallastOffset + 3;
				}
				if(unDataToBit.ucBit3 == 1)
				{
					stcDaliCnfBalAddDelGrpTemp.ucIDs_a[3] = ucAdresseBallastOffset + 4;
				}
				if(unDataToBit.ucBit4 == 1)
				{
					stcDaliCnfBalAddDelGrpTemp.ucIDs_a[4] = ucAdresseBallastOffset + 5;
				}
				if(unDataToBit.ucBit5 == 1)
				{
					stcDaliCnfBalAddDelGrpTemp.ucIDs_a[5] = ucAdresseBallastOffset + 6;
				}
				if(unDataToBit.ucBit6 == 1)
				{
					stcDaliCnfBalAddDelGrpTemp.ucIDs_a[6] = ucAdresseBallastOffset + 7;
				}
				if(unDataToBit.ucBit7 == 1)
				{
					stcDaliCnfBalAddDelGrpTemp.ucIDs_a[7] = ucAdresseBallastOffset + 8;
				}

				if(stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucDtr1 == CMD_AJOUT_BALLAST_GROUPE)
				{
					SetAddGroupeDaliRuntime(&stcDaliCnfBalAddDelGrpTemp);
				}
				else if(stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucDtr1 == CMD_SUPP_BALLAST_GROUPE)
				{
					SetDelGroupeDaliRuntime(&stcDaliCnfBalAddDelGrpTemp);
				}
			}
		}break;

		case(CMD_DALI_TEST_GROUPE_LAMPE_DALI):
		{
			if( (ucMsbAcc == 0x00) || (ucMsbAcc == 0x01) )
			{
				stcDaliCnfTestGroupe_t stcDaliCnfTestGroupeTemp;

				stcDaliCnfTestGroupeTemp.ucStateTestBallast = ucMsbAcc;

				if( (stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb >= 0) && (stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb <= 15) )
				{
					stcDaliCnfTestGroupeTemp.ucGroupeAddress = ( (stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb << 1)+1) + 0x80;
					SetTestDaliGroupeRuntime(&stcDaliCnfTestGroupeTemp);
				}
				else if(stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb == 0xFF)
				{
					stcDaliCnfTestGroupeTemp.ucGroupeAddress = stcDaliGenericRxFrame_pt->stcDaliAccRxFrame.ucLsb;
					SetTestDaliGroupeRuntime(&stcDaliCnfTestGroupeTemp);
				}

			}
		}break;
	}
#endif
}






/******************************************************************************/
/*!\brief SetTestDaliBallastRuntime
 * \author Jérémy BLEIN
 *
 * \param       stcDaliCnfTestBallast_t *stcDaliCnfTestBallast_pt.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetTestDaliBallastRuntime(stcDaliCnfTestBallast_t *stcDaliCnfTestBallast_pt)
{
#if(0) //JBL
	if( (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_STANDBY)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_GROUPE_TYPE)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_BALLAST_TYPE) )
	{
		memcpyST((uint8_t*)&stcDaliCnfTestBallast,(const uint8_t *)stcDaliCnfTestBallast_pt,SIZE_OF_CNF_BAL_TEST_BALLAST);
		ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_TEST_BALLAST_TYPE;
	}
#endif
}

/******************************************************************************/
/*!\brief SetTestDaliGroupeRuntime
 * \author Jérémy BLEIN
 *
 * \param       stcDaliCnfTestGroupe_t *stcDaliCnfTestGroupe_pt.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetTestDaliGroupeRuntime(stcDaliCnfTestGroupe_t *stcDaliCnfTestGroupe_pt)
{
#if(0) //JBL
	if( (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_STANDBY)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_GROUPE_TYPE)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_BALLAST_TYPE) )
	{
		memcpyST((uint8_t*)&stcDaliCnfTestGroupe,(const uint8_t *)stcDaliCnfTestGroupe_pt,SIZE_OF_CNF_BAL_TEST_GROUPE);
		ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_TEST_GROUPE_TYPE;
	}
#endif
}
