
#include <string.h>
#include "utilities.h"
//#include "stm32g0xx_hal.h"
#include "stm32f0xx_hal.h"

#include "DALI.h"
#include "DALI_APP.h"
#include "fifos.h"
#include "tick.h"
//#include "sheduler.h"
//#include "stm32g0xx_it.h"
#include "stm32f0xx_it.h"
#include "config.h"
//#include "RoomControl.h"
//#include "output.h"

#define TIME_VALIDATION_TX (TICK_SECOND/500)
#define TIME_LINE_OFF (TICK_SECOND*3)
#define MASK_ADDR_DALI										0x3F
#define MASK_MSB_DALI										0xC0

#define TEST_BALLAST_START									0x01
#define TEST_BALLAST_STOP									0x00

typedef enum  {
    DALI_FSM_ADD_DEL_G_INIT,
	DALI_FSM_ADD_DEL_G_OFF,
	DALI_FSM_ADD_DEL_G_LOCK_ACC,
	DALI_FSM_ADD_DEL_G_ADD,
	DALI_FSM_ADD_DEL_G_DEL,
	DALI_FSM_ADD_DEL_G_TERMINATE
} enDaliFsmAddDelGState_t;

typedef enum  {
    DALI_FSM_INV_INIT,
	DALI_FSM_INV_OFF,
	DALI_FSM_INV_LOCK_ACC,
	DALI_FSM_INV_SEARCH,
	DALI_FSM_INV_FIND,
	DALI_FSM_INV_STEP1,
	DALI_FSM_INV_QUERY1,
	DALI_FSM_INV_STEP2,
	DALI_FSM_INV_QUERY2,
	DALI_FSM_INV_STEP3,
	DALI_FSM_INV_QUERY3,
	DALI_FSM_INV_STEP4,
	DALI_FSM_INV_TERMINATE
} enDaliFsmInvState_t;

typedef union
{
    uint32_t uiBuffer;  	// soit 4 octets = 32 bits
    struct
    {
        // l'ordre est primordial, � savoir dans un long int, le LSByte est � l'adresse Buffer+0
        //                                                       MSByte                 Buffer+3
        //                                  dans chaque Byte, le LSBit  est le 1� bit d�clar�
        //                                                       MSBit         8�
    	uint8_t ucAddrL:8;		//LSB
        uint8_t ucAddrM:8;
        uint8_t ucAddrH:8;
        uint8_t ucNonUtilise:8;	 //MSB
	}stcOctet;
}unuiSearchAddress_t;


typedef union
{
	uint8_t ucDataToBit;
	struct
	{
		uint8_t ucBit0:1;
		uint8_t ucBit1:1;
		uint8_t ucBit2:1;
		uint8_t ucBit3:1;
		uint8_t ucBit4:1;
		uint8_t ucBit5:1;
		uint8_t ucBit6:1;
		uint8_t ucBit7:1;
	};
}unucDataToBit_t;




static uint8_t ucDefaultPercentTolerance = 17; /*value of tolerance between 2front in % (12% indication from Norme)*/
static uint8_t ucDefaultBitStopPeriod = 4;/*ms*/
static uint8_t ucDefaultInterPacketRxPeriod = 31;/*ms*/
static uint8_t ucDefaultInterFrameTxPeriod = 15;/*ms*/
static uint8_t ucDefaultInterPacketTxPeriod = 40;/*ms*/
static uint8_t ucDefaultCorrectionOpto = 14;/*microS*/
static uint32_t uiDefaultAddrAutoAndRemplTimeOut = 360000;/*ms*/





enDaliRxFsmState_t enDaliRxFsmState = DALI_RX_FSM_INIT;
enDaliRxFsmBitDecoder_t enDaliRxFsmBitDecoder = DALI_RX_FSM_B0;

static stcDaliGenericRxFrame_t stcDaliGenericRxFrame;


static stcDaliAccRxFrame_t stcDaliAccRxFrame;


enDaliRxDispatcherState_t enDaliRxDispatcherState = DALI_RX_DISP_STATE_INIT;

uint32_t uiReceptionBit = 0;



uint8_t ucRxDaliFront;


TIM_HandleTypeDef timDaliCptBetweenFront;
TIM_HandleTypeDef timDaliFrontTransitionTx;


uint8_t ucAddrAutoDali = USER_FALSE;
uint8_t ucBlocageTxDali = USER_FALSE;
uint8_t ucBlocageReelRxDali = USER_FALSE;
uint8_t ucTxDaliInProgres = USER_FALSE;
uint8_t ucRxCalibrationProcessing = USER_FALSE;

uint8_t	ucPercentageTolerance = USER_NO_VALUE;
uint8_t	ucCorrectionOpto = USER_NO_VALUE;
uint16_t	usCorrectionOptoCalibration = USER_NO_VALUE;

uint32_t uiTabTmpFrameRx[NUMBER_MAX_FRAME] = {0};




uint8_t ucflagtim6 = 0;


uint16_t usTimerSaveTimeCount[NUMBER_TIMER_DALI_MS] = {0};
uint8_t ucTimerSaveState[NUMBER_TIMER_DALI_MS] = {STOP_TIMER};

uint8_t ucCptFrameRx = 0;

/*
 *
 * TX Variable begin
 *
 *
 * */
uint8_t	ucTxDaliAdr;
uint8_t ucTxDaliCmd;
unFrameDali_t unFrameDali = {0};
uint8_t ucCntBitsTxDali = USER_NO_VALUE;
uint8_t *ucFrameDaliBuffer_pt;
uint8_t ucFlagStateManchester = USER_NO_VALUE;
uint8_t ucTxDaliBitsSize = NB_BITS_FRAME_DALI_REQUEST;
enDaliTxFsmState_t enDaliTxFsmState = DALI_TX_FSM_INIT;
enDaliFsmAdrState_t enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INIT;
stcDaliTxFrame_t stcDaliTxFrame = {0};
uint8_t ucCurrentFrameTx = NB_FRAME_MAX;
uint8_t ucFlagWaitResponseBalast = USER_FALSE;
uint8_t ucNbCmdFrameRep = 0 ;/*Cmd to be send twice*/
uint8_t ucMaskBitFrameTxDali = 0;
uint32_t uiTransmitBits = 0;
enDaliTxFrameType_t enDaliTxFrameType;
uint8_t ucFlagTimInterPacketTx = USER_FALSE;
uint8_t ucFlagTimInterFrameTx = USER_FALSE;


/*
 *
 *
 * TX Variable end
 *
 *
 * */

/*
 *
 *
 * Addr Auto Variable begin
 *
 *
 * */

unSearchAddress_t unSearchAddressMax;
unSearchAddress_t unSearchAddressMin;
unSearchAddress_t unSearchAddress;

uint8_t ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
/* Flag qui � 3 etats:
 *  DALI_COMPARE_ADDR_AUTO_IN_PROGRESS,
	DALI_COMPARE_ADDR_AUTO_RX_NOK,
	DALI_COMPARE_ADDR_AUTO_RX_OK

	� initialis� avant chaque compare � DALI_COMPARE_ADDR_AUTO_IN_PROGRESS

	change tout seul au bout de 15 ms


	le flag qui va permettre de bloquer la machine detat*/

uint8_t ucFlagCompareAddrAutoState = DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
/* Flag qui � 2 etats:
    DALI_COMPARE_ADDR_AUTO_STATE_RX_OK,
	DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK

	� initialis� avant chaque compare � DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK

	change tout seul si RX dali addr auto*/

uint8_t ucFlagWaitStartAddrAuto = USER_FALSE;
/* Flag qui permet de savoir si on demarre l'addr auto,
 * USER_FALSE = pas d'addr auto USER_TRUE = addr auto demand�
 * mise � jour par la SPI*/




enDaliBallastConfigType_t enDaliBallastConfigType = DALI_BALLAST_CONF_STANDBY ;
enDaliFsmAddDelGState_t enDaliFsmAddDelGState = DALI_FSM_ADD_DEL_G_INIT;
enDaliFsmInvState_t enDaliFsmInvState = DALI_FSM_INV_INIT;
enDaliFsmReplState_t enDaliFsmReplState = DALI_FSM_REPL_INIT;
enDaliFsmTestBallast_t enDaliFsmTestBallast = DALI_FSM_TEST_BALLAST_INIT;
enDaliFsmTestBallastGroupe_t enDaliFsmTestBallastGroupe = DALI_FSM_TEST_BALLAST_GROUPE_INIT;

/*
 *
 *
 * Addr Auto Variable end
 *
 *
 * */



uint32_t uiTestTab[16] = {0};
uint8_t ucTestIncTab = 0;

static uint8_t ucValueFeedBackBallast = 0;

static enDaliFsmWatchLineState_t enDaliFsmWatchLineState = DALI_FSM_WATCH_LINE_INIT;
uint8_t ucLineReady_g = DALI_LINE_NOK;

static uint16_t usTimeAccReTx = 350; /*350 correspond � 200 ms car il faut prendre en compte le temps d'�missions*/

void BuildFrameDaliTx(void);
static void DaliFsmAdrAutoTask(void);
static void DaliFsmInvTask(void);
static void DaliFsmAddDelGTask(void);
static void DaliFsmTestBallastTask(void);
static void DaliFsmTestGroupeBallastTask(void);
void DaliFsmReplTask(void);


/******************************************************************************/
/*!\brief DaliFsmRx
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *						Fini state machine for DALI
 ******************************************************************************/
void DaliFsmRx(void)
{
	static uint8_t ucPreviousFront;
	static uint32_t uiCptTimDaliCptBetweenFront;
	static uint32_t uiCptToleranceStartBitLow;
	static uint32_t uiCptToleranceStartBitHigh;
	static uint32_t uiCptToleranceOtherBitLow;
	static uint32_t uiCptToleranceOtherBitHigh;
#ifdef DALI_MASTER
	static stcSPIArcomFrame_t stcSpiArcomFrame = {0};
#endif

	switch(enDaliRxFsmState)
	{
		case DALI_RX_FSM_INIT : /* Do nothing */;
		break;
		case DALI_RX_FSM_ATTENTE :
			if(ucRxDaliFront == DALI_FRONT_DOWN)
			{
				if(ucAddrAutoDali == USER_TRUE)
				{
					enDaliRxFsmState = DALI_RX_FSM_ADDR_AUTO;
					ucBlocageTxDali = USER_TRUE;
					uiReceptionBit = 0;
				}
				else if(ucAddrAutoDali == USER_FALSE)
				{
					if(ucRxCalibrationProcessing == USER_TRUE)
					{
						__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);
						if (HAL_TIM_Base_Start(&timDaliCptBetweenFront) != HAL_OK)
			  			{
			    			/* Starting Error */
			    			Error_Handler();
			  			}

						enDaliRxFsmState = DALI_RX_FSM_CALIBRATION;
						ucBlocageTxDali = USER_TRUE;
						ucRxCalibrationProcessing = USER_FALSE;
					}
					else if(ucRxCalibrationProcessing == USER_FALSE)
					{
						if(ucTxDaliInProgres == USER_TRUE)
						{
							ucBlocageReelRxDali = USER_TRUE;
						}
						else if(ucTxDaliInProgres == USER_FALSE)
						{
							ucBlocageReelRxDali = USER_FALSE;
							ucBlocageTxDali = USER_TRUE;
						}

						__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);
						if (HAL_TIM_Base_Start(&timDaliCptBetweenFront) != HAL_OK)
			  			{
			    			/* Starting Error */
			    			Error_Handler();
			  			}

						enDaliRxFsmState = DALI_RX_FSM_START;
					}
				}

				TimerDali(TIMER_DALI_INTER_TRAM, STOP_TIMER, TIM_DALI_INTER_TRAM_PERIOD);


				ucPercentageTolerance = PERCENTAGE_TOLERANCE;


				ucCorrectionOpto = CORRECTION_OPTO_DALI;

				ucPreviousFront = ucRxDaliFront;

				uiCptToleranceStartBitLow = START_BIT_DELAY - ((START_BIT_DELAY*ucPercentageTolerance)/100);
				uiCptToleranceStartBitHigh = START_BIT_DELAY + ((START_BIT_DELAY*ucPercentageTolerance)/100);
				uiCptToleranceOtherBitLow = OTHER_BIT_DELAY - ((OTHER_BIT_DELAY*ucPercentageTolerance)/100);
				uiCptToleranceOtherBitHigh = OTHER_BIT_DELAY + ((OTHER_BIT_DELAY*ucPercentageTolerance)/100);

				enDaliRxFsmBitDecoder = DALI_RX_FSM_B0;/*Initialize the other FSM for decoder bit*/
				uiCptTimDaliCptBetweenFront = 0;
			}
		break;
		case DALI_RX_FSM_START :

			TimerDali(TIMER_DALI_BIT_STOP, START_RESET_TIMER, TIM_DALI_BIT_STOP_PERIOD);

			if(ucRxDaliFront == DALI_FRONT_UP)
			{

				uiCptTimDaliCptBetweenFront = __HAL_TIM_GET_COUNTER(&timDaliCptBetweenFront);
				uiCptTimDaliCptBetweenFront = OffsetOptoDali(ucPreviousFront,ucRxDaliFront,uiCptTimDaliCptBetweenFront);


				if((uiCptTimDaliCptBetweenFront >= uiCptToleranceStartBitLow) &&
						(uiCptTimDaliCptBetweenFront <= uiCptToleranceStartBitHigh))
				{
					__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);
					enDaliRxFsmState = DALI_RX_FSM_BIT_DECODER;
					ucPreviousFront = ucRxDaliFront;
				}
				else if(uiCptTimDaliCptBetweenFront > uiCptToleranceStartBitHigh)
				{
					TimerDali(TIMER_DALI_BIT_STOP, START_RESET_TIMER, TIM_DALI_BIT_STOP_PERIOD);

					enDaliRxFsmState = DALI_RX_FSM_ERREUR;
					if (HAL_TIM_Base_Stop(&timDaliCptBetweenFront) != HAL_OK)
		  			{
		    			/* Starting Error */
		    			Error_Handler();
		  			}
					__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);
				}
			}
		break;
		case DALI_RX_FSM_BIT_DECODER :

			TimerDali(TIMER_DALI_BIT_STOP, START_RESET_TIMER, TIM_DALI_BIT_STOP_PERIOD);

			uiCptTimDaliCptBetweenFront = __HAL_TIM_GET_COUNTER(&timDaliCptBetweenFront);
			uiCptTimDaliCptBetweenFront = OffsetOptoDali(ucPreviousFront,ucRxDaliFront,uiCptTimDaliCptBetweenFront);

			if((uiCptTimDaliCptBetweenFront >= uiCptToleranceOtherBitLow) &&
					(uiCptTimDaliCptBetweenFront <= uiCptToleranceOtherBitHigh))
			{
				__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);

				switch(enDaliRxFsmBitDecoder)
				{
					case DALI_RX_FSM_B0 :
						uiReceptionBit = 0;
						enDaliRxFsmBitDecoder = DALI_RX_FSM_B1;
					break;
					case DALI_RX_FSM_B1 :
					case DALI_RX_FSM_B2 :
					case DALI_RX_FSM_B3 :
					case DALI_RX_FSM_B4 :
					case DALI_RX_FSM_B5 :
					case DALI_RX_FSM_B6 :
					case DALI_RX_FSM_B7 :
					case DALI_RX_FSM_B8 :
					case DALI_RX_FSM_B9 :
					case DALI_RX_FSM_B10 :
					case DALI_RX_FSM_B11 :
					case DALI_RX_FSM_B12 :
					case DALI_RX_FSM_B13 :
					case DALI_RX_FSM_B14 :
					case DALI_RX_FSM_B15 :
					case DALI_RX_FSM_B16 :
					case DALI_RX_FSM_B17 :
					case DALI_RX_FSM_B18 :
					case DALI_RX_FSM_B19 :
					case DALI_RX_FSM_B20 :
					case DALI_RX_FSM_B21 :
					case DALI_RX_FSM_B22 :
					case DALI_RX_FSM_B23 :
						uiReceptionBit = uiReceptionBit << 1;
						enDaliRxFsmBitDecoder++;
					break;
					case DALI_RX_FSM_B24 :
						enDaliRxFsmState = DALI_RX_FSM_ERREUR;
					break;
					default:;
				}

				uiReceptionBit = uiReceptionBit + ucRxDaliFront;

				ucPreviousFront = ucRxDaliFront;
			}
			else if(uiCptTimDaliCptBetweenFront > uiCptToleranceOtherBitHigh)
			{
				enDaliRxFsmState = DALI_RX_FSM_ERREUR;

				if (HAL_TIM_Base_Stop(&timDaliCptBetweenFront) != HAL_OK)
	  			{
	    			/* Starting Error */
	    			Error_Handler();
	  			}
				__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);
			}

		break;
		case DALI_RX_FSM_CALIBRATION :

			usCorrectionOptoCalibration = 416 - (__HAL_TIM_GET_COUNTER(&timDaliCptBetweenFront));
			enDaliRxFsmState = DALI_RX_FSM_ATTENTE;
			ucBlocageTxDali = USER_FALSE;
			if (HAL_TIM_Base_Stop(&timDaliCptBetweenFront) != HAL_OK)
  			{
    			/* Starting Error */
    			Error_Handler();
  			}
			__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);
#ifdef DALI_MASTER
            stcSpiArcomFrame.ucCode = SYSTEM_CALIBRATION_OPTO_DALI;
            stcSpiArcomFrame.uData[0] = usCorrectionOptoCalibration >> 8;
            stcSpiArcomFrame.uData[1] = usCorrectionOptoCalibration;
            AddCrc16ToFrame((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE);
            Push((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE,SPI_TX_FIFO);
#endif
/*
 *
 * Prevoir le TX SPI
 *
 * */
		break;
		case DALI_RX_FSM_ADDR_AUTO :

			TimerDali(TIMER_DALI_BIT_STOP, START_RESET_TIMER, TIM_DALI_BIT_STOP_PERIOD);

			if((uiReceptionBit !=0xFFFFFFFF)  &&
					(ucFlagCompareAddrAuto == DALI_COMPARE_ADDR_AUTO_IN_PROGRESS))
			{
#ifdef DALI_MASTER
				stcDaliGenericRxFrame.ucCodeTypeRsp = DALI_TYPE_RSP_BALLAST;
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr1 = 0xFF;
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr = 0;
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucAddrDest = 0;
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucLsb = 0;
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucMsb = 0;

				Push((uint8_t*)&stcDaliGenericRxFrame,SIZE_DALI_RX_FRAME,DALI_RX_FIFO);
#endif

			}

			uiReceptionBit = 0xFFFFFFFF;

		break;
		case DALI_RX_FSM_ERREUR :

			TimerDali(TIMER_DALI_BIT_STOP, START_RESET_TIMER, TIM_DALI_BIT_STOP_PERIOD);

		break;
		default:;
	}
}

/******************************************************************************/
/*!\brief DecoderFrameDaliRx
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void DecoderFrameDaliRx(void)
{
	//uiTabTmpFrameRx

	uint8_t ucAddr = 0;
	uint8_t ucValue = 0;
	uint8_t ucFlagValueOk = USER_FALSE;
	uint8_t ucFlagShortFrameIndex = USER_NO_VALUE;



	if(enDaliRxFsmBitDecoder == DALI_RX_FSM_B8)
	{
		ucAddr = uiReceptionBit >> 8;
		ucValue = uiReceptionBit;

		if(ucCptFrameRx == 0)
		{
			uiTabTmpFrameRx[DTR1_DALI_RX_INDEX] = 0;
			uiTabTmpFrameRx[DTR_DALI_RX_INDEX] = 0;
			uiTabTmpFrameRx[ADDR_DALI_RX_INDEX] = 0;
			uiTabTmpFrameRx[LSB_DALI_RX_INDEX] = ucValue;
			uiTabTmpFrameRx[MSB_DALI_RX_INDEX] = 1;

			stcDaliGenericRxFrame.ucCodeTypeRsp = DALI_TYPE_RSP_BALLAST;
			stcDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr1 = ucValue;
			stcDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr = 0;
			stcDaliGenericRxFrame.stcDaliAccRxFrame.ucAddrDest = 0;
			stcDaliGenericRxFrame.stcDaliAccRxFrame.ucLsb = 0;
			stcDaliGenericRxFrame.stcDaliAccRxFrame.ucMsb = 0;

			Push((uint8_t*)&stcDaliGenericRxFrame,SIZE_DALI_RX_FRAME,DALI_RX_FIFO);
			/*
			 *
			 * uiTabTmpFrameRx[] a envoyer dans la pile avec un indicateur qui montre que c'est une reponse courte
			 *
			 *
			 * */
		}
		else
		{
			ucCptFrameRx = STOP_LECTURE;
		}
	}
	else if(enDaliRxFsmBitDecoder == DALI_RX_FSM_B16)
	{
		ucAddr = uiReceptionBit >> 8;
		ucValue = uiReceptionBit;

		if(ucCptFrameRx < STOP_LECTURE)
		{
			ucCptFrameRx++;
		}

		if((ucCptFrameRx == 1) && (ucAddr != CMD_DALI_STORE_IN_DTR1))
		{/*SHORT RESPONSE, ONLY ONE FRAME OF 16 BITS*/
			ucFlagValueOk = USER_FALSE;
			ucFlagShortFrameIndex = USER_NO_VALUE;

			if((ucAddr & 0x01) == 0)
			{
				ucFlagValueOk = USER_TRUE;
				ucFlagShortFrameIndex = SHORT_RESPONSE_TYPE_1;
			}
			else if(ucAddr == CMD_DALI_STORE_IN_DTR)
			{
				ucFlagValueOk = USER_TRUE;
				ucFlagShortFrameIndex = SHORT_RESPONSE_TYPE_2;
			}
			else if((ucAddr <=0x7F)&&
					((ucValue ==CMD_DALI_STORE_DTR_AS_FADE_TIME) || (ucValue ==CMD_DALI_STORE_DTR_AS_FADE_RATE)||
					(ucValue ==CMD_DALI_STORE_DTR_AS_MAX_LVL) || (ucValue ==CMD_DALI_STORE_DTR_AS_MIN_LVL)||
					(ucValue ==CMD_DALI_RECALL_MAX_LEVEL) || (ucValue ==CMD_DALI_RECALL_MIN_LEVEL)||
					(ucValue ==CMD_DALI_SET_OFF) || (ucValue ==CMD_DALI_RESET)))
			{
				ucFlagValueOk = USER_TRUE;
				ucFlagShortFrameIndex = SHORT_RESPONSE_TYPE_3;
			}
			else if(((ucAddr ==0xFF) || (ucAddr ==0x81)||
					(ucAddr ==0x83) || (ucAddr ==0x85)||
					(ucAddr ==0x87) || (ucAddr ==0x89)||
					(ucAddr ==0x8B) || (ucAddr ==0x8D)||
					(ucAddr ==0x8F) || (ucAddr ==0x91)||
					(ucAddr ==0x93) || (ucAddr ==0x95)||
					(ucAddr ==0x97) || (ucAddr ==0x99)||
					(ucAddr ==0x9B) || (ucAddr ==0x9D)||
					(ucAddr ==0x9F)) &&
					((ucValue ==CMD_DALI_STORE_DTR_AS_FADE_TIME) || (ucValue ==CMD_DALI_STORE_DTR_AS_FADE_RATE)||
					(ucValue ==CMD_DALI_STORE_DTR_AS_MAX_LVL) || (ucValue ==CMD_DALI_STORE_DTR_AS_MIN_LVL)||
					(ucValue ==CMD_DALI_RECALL_MAX_LEVEL) || (ucValue ==CMD_DALI_RECALL_MIN_LEVEL)||
					(ucValue ==CMD_DALI_SET_OFF) || (ucValue ==CMD_DALI_RESET)))
			{
				ucFlagValueOk = USER_TRUE;
				ucFlagShortFrameIndex = SHORT_RESPONSE_TYPE_3;
			}

			uiTabTmpFrameRx[ucCptFrameRx-1] = ucFlagShortFrameIndex;
			uiTabTmpFrameRx[ucCptFrameRx] = ucAddr;
			uiTabTmpFrameRx[ucCptFrameRx+1] = ucValue;

		}
		else if((ucCptFrameRx == 1) && (ucAddr == CMD_DALI_STORE_IN_DTR1))
		{
			uiTabTmpFrameRx[DTR1_DALI_RX_INDEX] = ucValue;
		}
		else if((ucCptFrameRx == 2) && (ucAddr == CMD_DALI_STORE_IN_DTR))
		{
			uiTabTmpFrameRx[DTR_DALI_RX_INDEX] = ucValue;
		}
		else if((ucCptFrameRx == 3) && (ucValue == CMD_DALI_ENABLE_WRITE_MEMORY))
		{
			ucAddr = (ucAddr-1) >> 1;
			uiTabTmpFrameRx[ADDR_DALI_RX_INDEX] = ucAddr;
		}
		else if((ucCptFrameRx == 4) && (ucValue == CMD_DALI_ENABLE_WRITE_MEMORY))
		{
			ucAddr = (ucAddr-1) >> 1;
			if(uiTabTmpFrameRx[ADDR_DALI_RX_INDEX] != ucAddr)
			{
				ucCptFrameRx = STOP_LECTURE;
			}
		}
		else if((ucCptFrameRx == 5) && (ucAddr == CMD_DALI_WRITE_MEMORY))
		{
			uiTabTmpFrameRx[LSB_DALI_RX_INDEX] = ucValue;
		}
		else if((ucCptFrameRx == 6) && (ucAddr == CMD_DALI_WRITE_MEMORY))
		{
			ucFlagValueOk = USER_TRUE;
			uiTabTmpFrameRx[MSB_DALI_RX_INDEX] = ucValue;
		}
		else
		{
			ucFlagValueOk = USER_FALSE;
			ucCptFrameRx = STOP_LECTURE;
		}

		if(ucFlagValueOk == USER_TRUE)
		{
			if(ucCptFrameRx == 1)
			{
				ucCptFrameRx = STOP_LECTURE;
				/*
				 *
				 * uiTabTmpFrameRx[] a envoyer dans la pile avec un indicateur qui montre que c'est une reponse courte
				 *
				 *
				 * */
			}
			else if (ucCptFrameRx == 6)
			{
				uiTestTab[0] = uiTabTmpFrameRx[0];
				uiTestTab[1] = uiTabTmpFrameRx[1];
				uiTestTab[2] = uiTabTmpFrameRx[2];
				uiTestTab[3] = uiTabTmpFrameRx[3];
				uiTestTab[4] = uiTabTmpFrameRx[4];
				ucCptFrameRx = STOP_LECTURE;

				stcDaliGenericRxFrame.ucCodeTypeRsp = DALI_TYPE_RSP_ACC;
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr1 = uiTabTmpFrameRx[DTR1_DALI_RX_INDEX];
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr = uiTabTmpFrameRx[DTR_DALI_RX_INDEX];
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucAddrDest = uiTabTmpFrameRx[ADDR_DALI_RX_INDEX];
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucLsb = uiTabTmpFrameRx[LSB_DALI_RX_INDEX];
				stcDaliGenericRxFrame.stcDaliAccRxFrame.ucMsb = uiTabTmpFrameRx[MSB_DALI_RX_INDEX];

				Push((uint8_t*)&stcDaliGenericRxFrame,SIZE_DALI_RX_FRAME,DALI_RX_FIFO);
				/*
				 *
				 * uiTabTmpFrameRx[] a envoyer dans la pile avec un indicateur qui montre que c'est une reponse longue
				 *
				 *
				 * */
			}
		}
		//uiTabTmpFrameRx[] = uiReceptionBit;
	}
	else if(enDaliRxFsmBitDecoder == DALI_RX_FSM_B24)
	{
		/*Do nothing*/
	}
}

/******************************************************************************/
/*!\brief CallBackTimDaliBitStop
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void CallBackTimDaliBitStop(void)
{
	uint8_t ucTmpState = 0;
	uint8_t ucI = 0;

	if((enDaliRxFsmBitDecoder == DALI_RX_FSM_B8) ||
			(enDaliRxFsmBitDecoder == DALI_RX_FSM_B24) ||
			(enDaliRxFsmBitDecoder == DALI_RX_FSM_B16))
	{
		if((enDaliRxFsmBitDecoder == DALI_RX_FSM_B8) ||
			(enDaliRxFsmBitDecoder == DALI_RX_FSM_B24))
		{
			ucBlocageTxDali = USER_FALSE;
		}
		else
		{
			TimerDali(TIMER_DALI_INTER_TRAM, START_RESET_TIMER, TIM_DALI_INTER_TRAM_PERIOD);
		}

		enDaliRxFsmState = DALI_RX_FSM_ATTENTE;

		if (HAL_TIM_Base_Stop(&timDaliCptBetweenFront) != HAL_OK)
		{
			/* Starting Error */
			Error_Handler();
		}
		__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);

		TimerDali(TIMER_DALI_BIT_STOP, STOP_TIMER, TIM_DALI_BIT_STOP_PERIOD);

		if(ucBlocageReelRxDali == USER_FALSE)
		{
			DecoderFrameDaliRx();
		}

	}
	else
	{
		if(enDaliRxFsmState == DALI_RX_FSM_ADDR_AUTO)
		{
			ucTmpState = HAL_GPIO_ReadPin(GPIO_DALI_RX_PORT,GPIO_DALI_RX_PIN);
			if(ucTmpState == USER_PIN_RX_DALI_HIGH)
			{
				enDaliRxFsmState = DALI_RX_FSM_ATTENTE;

				TimerDali(TIMER_DALI_BIT_STOP, STOP_TIMER, TIM_DALI_BIT_STOP_PERIOD);

				ucBlocageTxDali = USER_FALSE;

				ucAddrAutoDali = USER_FALSE;
			}
			else
			{
				TimerDali(TIMER_DALI_BIT_STOP, START_RESET_TIMER, TIM_DALI_BIT_STOP_PERIOD);
			}
		}
		else
		{
			enDaliRxFsmState = DALI_RX_FSM_ATTENTE;

			ucCptFrameRx = 0;
			uiTestTab[14]++;
			uiTestTab[1] = uiReceptionBit;
			for(ucI=0;ucI<NUMBER_MAX_FRAME;ucI++)
			{
				uiTabTmpFrameRx[ucI] = 0;
			}

			if (HAL_TIM_Base_Stop(&timDaliCptBetweenFront) != HAL_OK)
			{
				/* Starting Error */
				Error_Handler();
			}
			__HAL_TIM_SET_COUNTER(&timDaliCptBetweenFront,0);

			TimerDali(TIMER_DALI_BIT_STOP, STOP_TIMER, TIM_DALI_BIT_STOP_PERIOD);


			TimerDali(TIMER_DALI_INTER_TRAM, START_RESET_TIMER, TIM_DALI_INTER_TRAM_PERIOD);
		}
	}
}

/******************************************************************************/
/*!\brief CallBackTimDaliInterTram
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void CallBackTimDaliInterTram(void)
{
	uint8_t ucI = 0;


	enDaliRxFsmState = DALI_RX_FSM_ATTENTE;

	ucBlocageTxDali = USER_FALSE;

	ucCptFrameRx = 0;

	for(ucI=0;ucI<NUMBER_MAX_FRAME;ucI++)
	{
		uiTabTmpFrameRx[ucI] = 0;
	}

	TimerDali(TIMER_DALI_INTER_TRAM, STOP_TIMER, TIM_DALI_INTER_TRAM_PERIOD);
}

/******************************************************************************/
/*!\brief InitDali
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void InitDali(void)
{
	stcSystemConfig_t stcSystemConfig ={0};
#ifdef DALI_MASTER
	stcSPIArcomFrame_t stcSpiArcomFrame ={0};
#endif
	uint32_t uiTmp = 0;

	GpioDaliInit();
	timDaliCptBetweenFrontInit();
	timDaliFrontTransitionTxInit();
	__HAL_TIM_CLEAR_IT(&timDaliCptBetweenFront, TIM_IT_UPDATE);
	__HAL_TIM_CLEAR_IT(&timDaliFrontTransitionTx, TIM_IT_UPDATE);

	enDaliRxFsmState = DALI_RX_FSM_ATTENTE;

	stcSystemConfig.ucSystemConfigBitStop = ucDefaultBitStopPeriod;
	stcSystemConfig.ucSystemConfigCorrectionOpto = ucDefaultCorrectionOpto;
	stcSystemConfig.ucSystemConfigInterFrameTx = ucDefaultInterFrameTxPeriod;
	stcSystemConfig.ucSystemConfigInterPacketRx = ucDefaultInterPacketRxPeriod;
	stcSystemConfig.ucSystemConfigInterPacketTx = ucDefaultInterPacketTxPeriod;
	stcSystemConfig.ucSystemConfigPercentTolerance = ucDefaultPercentTolerance;
	uiTmp = uiDefaultAddrAutoAndRemplTimeOut;
	uiTmp = uiTmp /1000;
	stcSystemConfig.ucSystemConfigLsbAddrAutoTimeOut = uiTmp;
	stcSystemConfig.ucSystemConfigMsbAddrAutoTimeOut = uiTmp >> 8;

#ifdef DALI_MASTER
	stcSpiArcomFrame.ucCode = SYSTEM_CONFIG;
	memcpyST((uint8_t*)stcSpiArcomFrame.uData,(const uint8_t *)&stcSystemConfig,sizeof(stcSystemConfig_t));
	AddCrc16ToFrame((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE);
	Push((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE,SPI_TX_FIFO);
#endif
}

/******************************************************************************/
/*!\brief GpioDaliInit
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void GpioDaliInit(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  GPOI_DALI_TX_ENABLE_CLK_PORT;
  GPOI_DALI_RX_ENABLE_CLK_PORT;

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIO_DALI_TX_PORT, GPIO_DALI_TX_PIN, GPIO_PIN_RESET);


  /*Configure GPIO pin : PD6 */
  GPIO_InitStruct.Pin = GPIO_DALI_TX_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;//GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIO_DALI_TX_PORT, &GPIO_InitStruct);

  /*Configure GPIO pin : PB3 */
  GPIO_InitStruct.Pin = GPIO_DALI_RX_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIO_DALI_RX_PORT, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(GPIO_EXTI_DALI_RX, PRIORITY_INTERRUPT_DALI_RX, PRIORITY_INTERRUPT_DALI_RX);
  HAL_NVIC_EnableIRQ(GPIO_EXTI_DALI_RX);

}

/******************************************************************************/
/*!\brief TimerCptBetweenFrontInit
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void timDaliCptBetweenFrontInit(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  timDaliCptBetweenFront.Instance = TIM_DALI_CPT_BETWEEN_FRONT_INSTANCE;
  timDaliCptBetweenFront.Init.Prescaler = TIM_DALI_CPT_BETWEEN_FRONT_PRESCALER;/*((freq of internal clock) / (freq we want to count)) -1*/
  timDaliCptBetweenFront.Init.CounterMode = TIM_COUNTERMODE_UP;
  timDaliCptBetweenFront.Init.Period = 0xFFFF;
  timDaliCptBetweenFront.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  timDaliCptBetweenFront.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&timDaliCptBetweenFront) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&timDaliCptBetweenFront, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&timDaliCptBetweenFront, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

/******************************************************************************/
/*!\brief timDaliFrontTransitionTxInit
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void timDaliFrontTransitionTxInit(void)
{
	  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
	  TIM_MasterConfigTypeDef sMasterConfig = {0};

	  /* USER CODE BEGIN TIM1_Init 1 */

	  /* USER CODE END TIM1_Init 1 */
	  timDaliFrontTransitionTx.Instance = TIM1;
	  timDaliFrontTransitionTx.Init.Prescaler = 47;
	  timDaliFrontTransitionTx.Init.CounterMode = TIM_COUNTERMODE_UP;
	  timDaliFrontTransitionTx.Init.Period = 416;
	  timDaliFrontTransitionTx.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	  timDaliFrontTransitionTx.Init.RepetitionCounter = 0;
	  timDaliFrontTransitionTx.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	  if (HAL_TIM_Base_Init(&timDaliFrontTransitionTx) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	  if (HAL_TIM_ConfigClockSource(&timDaliFrontTransitionTx, &sClockSourceConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	  if (HAL_TIMEx_MasterConfigSynchronization(&timDaliFrontTransitionTx, &sMasterConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }
}


//void TIM1_BRK_UP_TRG_COM_IRQHandler(void)
//{
//  /* USER CODE BEGIN TIM1_BRK_UP_TRG_COM_IRQn 0 */
//
//  /* USER CODE END TIM1_BRK_UP_TRG_COM_IRQn 0 */
//  HAL_TIM_IRQHandler(&timDaliFrontTransitionTx);
//  /* USER CODE BEGIN TIM1_BRK_UP_TRG_COM_IRQn 1 */
// // HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_14);
//
//  /* USER CODE END TIM1_BRK_UP_TRG_COM_IRQn 1 */
//}

//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
//{
//
//}





/******************************************************************************/
/*!\brief VOID_FUNCTION_PIN_RX_DALI_INTERRUPT_VOID
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *						Interruption RX DALI front up and down
 ******************************************************************************/
VOID_FUNCTION_PIN_RX_DALI_INTERRUPT_VOID
{
#if(1) //JBL
	if(ucTxDaliInProgres == USER_FALSE)
	{

		  /* EXTI line interrupt detected */
		  if (__HAL_GPIO_EXTI_GET_RISING_IT(GPIO_DALI_RX_PIN) != 0x00u)/* Front Descandant */
		  {
			  ucRxDaliFront = DALI_FRONT_DOWN;
			  __HAL_GPIO_EXTI_CLEAR_RISING_IT(GPIO_DALI_RX_PIN);
			  HAL_GPIO_EXTI_Rising_Callback(GPIO_DALI_RX_PIN);
			DaliFsmRx();
		  }

		  if (__HAL_GPIO_EXTI_GET_FALLING_IT(GPIO_DALI_RX_PIN) != 0x00u)/* Front Montant */
		  {
			  ucRxDaliFront = DALI_FRONT_UP;
			  __HAL_GPIO_EXTI_CLEAR_FALLING_IT(GPIO_DALI_RX_PIN);
			  HAL_GPIO_EXTI_Falling_Callback(GPIO_DALI_RX_PIN);
			  DaliFsmRx();
		  }
	}
//	else
//	{
//		  /* EXTI line interrupt detected */
//		  if (__HAL_GPIO_EXTI_GET_RISING_IT(GPIO_DALI_RX_PIN) != 0x00u)/* Front Descandant */
//		  {
//			  //ucRxDaliFront = DALI_FRONT_DOWN;
//			__HAL_GPIO_EXTI_CLEAR_RISING_IT(GPIO_DALI_RX_PIN);
//			HAL_GPIO_EXTI_Rising_Callback(GPIO_DALI_RX_PIN);
//			//DaliFsmRx();
//		  }
//
//		  if (__HAL_GPIO_EXTI_GET_FALLING_IT(GPIO_DALI_RX_PIN) != 0x00u)/* Front Montant */
//		  {
//			  //ucRxDaliFront = DALI_FRONT_UP;
//			__HAL_GPIO_EXTI_CLEAR_FALLING_IT(GPIO_DALI_RX_PIN);
//			HAL_GPIO_EXTI_Falling_Callback(GPIO_DALI_RX_PIN);
//			//DaliFsmRx();
//		  }
//	}
#endif
}

/******************************************************************************/
/*!\brief TimerDali
 * \author Jimmy ZANOTTO
 *
 * \param       uint8_t ucTimerName
 * 				uint8_t ucState
 * 				 uint8_t usTimeCount
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void TimerDali(uint8_t ucTimerName, uint8_t ucState, uint16_t usTimeCount)
{
	if(ucTimerName < NUMBER_TIMER_DALI_MS)
	{/*security*/

		if(ucState == START_RESET_TIMER)
		{
			ucTimerSaveState[ucTimerName] = START_RESET_TIMER;
			usTimerSaveTimeCount[ucTimerName] = usTimeCount;
		}
		else if(ucState == STOP_TIMER)
		{
			ucTimerSaveState[ucTimerName] = STOP_TIMER;
		}
	}

}

/******************************************************************************/
/*!\brief TimerScheduler
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
#ifdef USE_SCHEDULER
void TimerDaliScheduler(en_change_behavior_fnt_t en_change_behavior_fnt)
#else
void TimerDaliScheduler(void)
#endif
{
	static uint32_t uiSaveTime[NUMBER_TIMER_DALI_MS] = {0};
	uint32_t uiTimeTmp = 0;
	uint8_t ucI = 0;

	for(ucI=0;ucI<NUMBER_TIMER_DALI_MS;ucI++)
	{
		if(ucTimerSaveState[ucI] == START_RESET_TIMER)
		{
			ucTimerSaveState[ucI] = PROCESSING_TIMER;
			uiSaveTime[ucI] = HAL_GetTick();
		}
		else if(ucTimerSaveState[ucI] == PROCESSING_TIMER)
		{
			uiTimeTmp = HAL_GetTick();

			if(uiTimeTmp < uiSaveTime[ucI])
			{/*protection of overflow*/
				uiSaveTime[ucI] = uiTimeTmp;
			}

			uiTimeTmp = uiTimeTmp - uiSaveTime[ucI];

			if(uiTimeTmp >= usTimerSaveTimeCount[ucI])
			{
				ucTimerSaveState[ucI] = STOP_TIMER;
				if(ucI == TIMER_DALI_BIT_STOP)
				{
					CallBackTimDaliBitStop();
				}
				else if(ucI == TIMER_DALI_INTER_TRAM)
				{
					CallBackTimDaliInterTram();
				}
				else if(ucI == TIMER_DALI_INTER_FRAME_TX)
				{
					CallBackTimInterFrameTx();
				}
				else if(ucI == TIMER_DALI_INTER_PACKET_TX)
				{
					CallBackTimInterPacketTx();
				}

			}
		}
	}
}
/******************************************************************************/
/*!\brief OffsetOptoDali
 * \author Jimmy ZANOTTO
 *
 * \param       uint8_t ucPreviousFrontTmp
 * 				 uint8_t ucActualFrontTmp
 * 				 uint32_t uiValueCptTimer
 *
 * \return      uint32_t
 *
 * long description :
 *
 ******************************************************************************/
uint32_t OffsetOptoDali(uint8_t ucPreviousFrontTmp, uint8_t ucActualFrontTmp, uint32_t uiValueCptTimer)
{
	if(ucPreviousFrontTmp != ucActualFrontTmp)
	{
		if((ucPreviousFrontTmp == DALI_FRONT_DOWN) && (ucActualFrontTmp == DALI_FRONT_UP))
		{
			uiValueCptTimer = uiValueCptTimer + ucCorrectionOpto;
		}
		else if((ucPreviousFrontTmp == DALI_FRONT_UP) && (ucActualFrontTmp == DALI_FRONT_DOWN))
		{
			if(uiValueCptTimer >= ucCorrectionOpto)
			{
				uiValueCptTimer = uiValueCptTimer - ucCorrectionOpto;
			}

		}
		return uiValueCptTimer;
	}
	else
	{
		return uiValueCptTimer;
	}
}

/******************************************************************************/
/*!\brief SetAddrAutoDali
 * \author Jimmy ZANOTTO
 *
 * \param       uint8_t ucTmp
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetAddrAutoDali(uint8_t ucTmp)
{
	ucAddrAutoDali = ucTmp;
}

/******************************************************************************/
/*!\brief GetBlocageTxDali
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      uint8_t
 *
 * long description :
 *
 ******************************************************************************/
uint8_t GetBlocageTxDali(void)
{
	return ucBlocageTxDali;
}

/******************************************************************************/
/*!\brief SetPercentageTolerance
 * \author Jimmy ZANOTTO
 *
 * \param       uint8_t ucTmp
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetPercentageTolerance(uint8_t ucTmp)
{
	ucPercentageTolerance = ucTmp;
}

/******************************************************************************/
/*!\brief SetCorrectionOpto
 * \author Jimmy ZANOTTO
 *
 * \param       uint8_t ucTmp
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetCorrectionOpto(uint8_t ucTmp)
{
	ucCorrectionOpto = ucTmp;
}

/******************************************************************************/
/*!\brief GetCorrectionOptoCalibration
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      uint8_t
 *
 * long description :
 *
 ******************************************************************************/
uint16_t GetCorrectionOptoCalibration(void)
{
	return usCorrectionOptoCalibration;
}


/******************************************************************************/
/*!\brief SetRxCalibrationProcessing
 * \author Jimmy ZANOTTO
 *
 * \param       uint8_t ucTmp
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetRxCalibrationProcessing(uint8_t ucTmp)
{
	if(ucTmp != USER_FALSE)
	{
		ucRxCalibrationProcessing = USER_TRUE;
	}
	else
	{
		ucRxCalibrationProcessing = USER_FALSE;
	}
}



/******************************************************************************/
/*!\brief DaliFsmTxTask
 * \author Jimmy ZANOTTO + Alejandro MEJIA
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
#ifdef USE_SCHEDULER
void DaliFsmTxTask(en_change_behavior_fnt_t en_change_behavior_fnt)
#else
void DaliFsmTxTask(void)
#endif
{
    static TICK uiLastTick_s;
    TICK uiCurrentTick_s;
    static TICK uiLastTickAcc_s = 0;
    TICK uiCurrentTickAcc_s = 0;
    uint16_t usBytesInFifo = 0;
    static uint8_t ucReTxAcc = 0;


    switch(enDaliTxFsmState)
    {
        case DALI_TX_FSM_INIT :
        	ucCurrentFrameTx = NB_FRAME_MAX;
        	ucNbCmdFrameRep = 0;
        	if(ucLineReady_g == DALI_LINE_OK)
        	{
            	enDaliTxFsmState = DALI_TX_FSM_WAIT_FIFO;
        	}
        break;
        case DALI_TX_FSM_WAIT_FIFO :

        	 usBytesInFifo = GetFifoNbBytes(DALI_TX_FIFO);

        	 usBytesInFifo = 6;//JBL

        	 if((usBytesInFifo < SIZE_DALI_TX_FRAME) &&
        			 (usBytesInFifo > 0) )
        	 {
#ifdef DEBUG_DALI	
        		dbgprint("%s -> Error frame size in fifo not ok\r\n",__FUNCTION__);
#endif
        	 }
        	 else
        	 {
        		 if(usBytesInFifo)
        		 {
#ifdef DEBUG_DALI	
					 dbgprint("%s -> Item in  fifo found\r\n",__FUNCTION__);
#endif
        			 enDaliTxFsmState = DALI_TX_FSM_IS_READY_TX;
        		 }
        	 }

        break;
        case DALI_TX_FSM_IS_READY_TX :
        	uiCurrentTickAcc_s = tickGet();
        	if(ucReTxAcc == 0)
        	{
            	if(ucBlocageTxDali == USER_FALSE)
            	{
            		ucNbCmdFrameRep = 0;
            		ucCurrentFrameTx = NB_FRAME_MAX;
            		enDaliTxFsmState = DALI_TX_FSM_PARSE_DATA;
            		ReadPop((uint8_t*)&stcDaliTxFrame,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);

            		stcDaliTxFrame.ucDtr1 = 44;
            		stcDaliTxFrame.ucCmd = CMD_DALI_ENABLE_WRITE_MEMORY;

            		if(stcDaliTxFrame.ucCmd == CMD_DALI_ENABLE_WRITE_MEMORY)
            		{
            			uiLastTickAcc_s = tickGet();
            			ucReTxAcc = 1;
            		}
            		else
            		{
            			uiLastTickAcc_s = tickGet();
            			ucReTxAcc = 0;
            		}
    #ifdef DEBUG_DALI
    				dbgprint("\r\n%s ->Tx done - Data in fifo :\r\n",__FUNCTION__);
    				dbgprint("stcDaliTxFrame.ucDtr1 : %d\r\n",stcDaliTxFrame.ucDtr1);
    				dbgprint("stcDaliTxFrame.ucDtr : %d\r\n",stcDaliTxFrame.ucDtr);
    				dbgprint("stcDaliTxFrame.ucCmd : %d\r\n",stcDaliTxFrame.ucCmd);
    				dbgprint("stcDaliTxFrame.ucLsb : %d\r\n",stcDaliTxFrame.ucLsb);
    				dbgprint("stcDaliTxFrame.ucMsb : %d\r\n",stcDaliTxFrame.ucMsb);
    				dbgprint("stcDaliTxFrame.ucAdresse : %d\r\n\n",stcDaliTxFrame.ucAdresse);
    #endif
            	}
        	}

        	if(uiLastTickAcc_s == 0)
        	{
        		uiLastTickAcc_s = tickGet();
        	}
        	else
            {
    			if(uiLastTickAcc_s > uiCurrentTickAcc_s)
    			{/*protection*/
    				uiLastTickAcc_s = uiCurrentTickAcc_s;
    			}

    			if(TICK_GET_DIFF(uiCurrentTickAcc_s,uiLastTickAcc_s) >= (usTimeAccReTx))
    			{
    				uiLastTickAcc_s = uiCurrentTickAcc_s;
    				ucReTxAcc = 0;
    			}
            }

        break;
        case DALI_TX_FSM_PARSE_DATA :
#ifdef DEBUG_DALI	
			dbgprint("%s -> Call parseDataDaliTx\r\n",__FUNCTION__);
#endif
        	ParseDataDaliTx();

        	enDaliTxFsmState = DALI_TX_FSM_BUILD_FRAME;

        break;
        case DALI_TX_FSM_BUILD_FRAME :
#ifdef DEBUG_DALI	
			dbgprint("%s -> Call BuildFrameDaliTx\r\n",__FUNCTION__);
#endif
        	BuildFrameDaliTx();

			ucCntBitsTxDali = 0;
			ucFlagStateManchester = 0;
			ucTxDaliBitsSize = NB_BITS_FRAME_DALI_REQUEST-2;
	        ucMaskBitFrameTxDali = 0x80;          	// MSB first
	        ucTxDaliInProgres = USER_TRUE;

			__HAL_TIM_SET_COUNTER(&timDaliFrontTransitionTx,0);
			if (HAL_TIM_Base_Start_IT(&timDaliFrontTransitionTx) != HAL_OK)
			{
				/* Starting Error */
				Error_Handler();
			}
        	enDaliTxFsmState = DALI_TX_FSM_WAIT_FOR_END_TX;
        break;
        case DALI_TX_FSM_WAIT_FOR_END_TX :

        	if(ucMaskBitFrameTxDali == 0)
        	{/*TX ucTxDaliBitsSize bits finish*/
#ifdef DEBUG_DALI	
			    dbgprint("%s -> DALI Tx frame transmitted\r\n",__FUNCTION__);
#endif
        		uiLastTick_s = tickGet();
        		enDaliTxFsmState = DALI_TX_FSM_WAIT_FOR_TX_VALIDATION;
        	}

        break;
        case DALI_TX_FSM_WAIT_FOR_TX_VALIDATION :

        	uiCurrentTick_s = tickGet();
        	uiCurrentTick_s = TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s);

        	if(uiCurrentTick_s >= TIME_VALIDATION_TX)
        	{
        		ucFlagTimInterPacketTx = USER_FALSE;
        		ucFlagTimInterFrameTx = USER_FALSE;

        		if(1/*uiReceptionBit == uiTransmitBits*/)/*TODO � finir lorsque l'on voudra comparer ce que l'on a envoyer*/
        		{
#ifdef DEBUG_DALI
					dbgprint("%s -> uiReceptionBit : %d - uiTransmitBits : %d ucNbCmdFrameRep %d - ucCurrentFrameTx :%d - enDaliTxFrameType %d - tick :%d\r\n",__FUNCTION__,uiReceptionBit,uiTransmitBits,ucNbCmdFrameRep,ucCurrentFrameTx,enDaliTxFrameType,tickGet());
#endif
        			if(ucNbCmdFrameRep != 0)
        			{
						/*15 ms*/
        				TimerDali(TIMER_DALI_INTER_FRAME_TX,START_RESET_TIMER,TIM_DALI_INTER_FRAME_TX_PERIOD);
        			}
        			else
        			{
        				if((enDaliTxFrameType == DALI_TX_PARSE_STATE_INTER_FRAME) ||
        						(enDaliTxFrameType == DALI_TX_PARSE_STATE_CMD_BALLAST) ||
								(enDaliTxFrameType == DALI_TX_PARSE_STATE_CMD_COMPARE))
        				{
							/*15 ms*/
        					TimerDali(TIMER_DALI_INTER_FRAME_TX,START_RESET_TIMER,TIM_DALI_INTER_FRAME_TX_PERIOD);

        					if((enDaliTxFrameType == DALI_TX_PARSE_STATE_CMD_BALLAST) ||
								(enDaliTxFrameType == DALI_TX_PARSE_STATE_CMD_COMPARE))
        					{
        						ucTxDaliInProgres = USER_FALSE;

        					}
        				}
        				else
        				{
							ucTxDaliInProgres = USER_FALSE;

							/*40 ms - inter packet*/
        					TimerDali(TIMER_DALI_INTER_PACKET_TX,START_RESET_TIMER,TIM_DALI_INTER_PACKET_TX_PERIOD);
        				}
        			}

        		}
        		else
        		{
					ucTxDaliInProgres = USER_FALSE;
					/*40 ms - inter packet*/
        			TimerDali(TIMER_DALI_INTER_PACKET_TX,START_RESET_TIMER,TIM_DALI_INTER_PACKET_TX_PERIOD);
        		}

        		enDaliTxFsmState = DALI_TX_FSM_WAIT_FOR_END_TIMER;
        	}

        break;
        case DALI_TX_FSM_WAIT_FOR_END_TIMER :

        	if((ucBlocageTxDali == USER_FALSE) && (GetFifoNbBytes(DALI_RX_FIFO) == 0))
        	{
        		if(ucFlagTimInterPacketTx == USER_TRUE)
        		{
    #ifdef DEBUG_DALI
    				dbgprint("%s -> WAIT_FOR_END_TIMER end packet - tick :%d\r\n",__FUNCTION__,tickGet());
    #endif
        			ucFlagTimInterPacketTx = USER_FALSE;
        			enDaliTxFsmState = DALI_TX_FSM_WAIT_FIFO;
    				Pop((uint8_t*)&stcDaliTxFrame,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);
    	            memset((uint8_t*)&stcDaliTxFrame,0,DALI_TX_FIFO);
        		}
        		else if(ucFlagTimInterFrameTx == USER_TRUE)
        		{
    #ifdef DEBUG_DALI
    				dbgprint("%s -> WAIT_FOR_END_TIMER end frame - tick :%d\r\n",__FUNCTION__,tickGet());
    #endif
        			ucFlagTimInterFrameTx = USER_FALSE;

        			if(ucNbCmdFrameRep != 0)
        			{
        				enDaliTxFsmState = DALI_TX_FSM_BUILD_FRAME;
        			}
        			else
        			{
        				switch(enDaliTxFrameType)
        				{
        					case DALI_TX_PARSE_STATE_INTER_FRAME:

        						enDaliTxFsmState = DALI_TX_FSM_PARSE_DATA;

        					break;
        					case DALI_TX_PARSE_STATE_CMD_BALLAST:

    							 if(ucFlagWaitResponseBalast == USER_TRUE)
    							 {
    								 ucFlagWaitResponseBalast = USER_FALSE;

    								 if(ucFlagCompareAddrAutoState == DALI_COMPARE_ADDR_AUTO_STATE_RX_OK)
    								 {
    									 ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_RX_OK;
    								 }
    								 else
    								 {
    									 ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_RX_NOK;
    								 }
    								 Pop((uint8_t*)&stcDaliTxFrame,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);
    						         memset((uint8_t*)&stcDaliTxFrame,0,DALI_TX_FIFO);
    								 enDaliTxFsmState = DALI_TX_FSM_WAIT_FIFO;
    							 }
    							 else
    							 {
    								 Pop((uint8_t*)&stcDaliTxFrame,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);
    						         memset((uint8_t*)&stcDaliTxFrame,0,DALI_TX_FIFO);
    								 enDaliTxFsmState = DALI_TX_FSM_WAIT_FIFO;
    							 }

        					break;
        					case DALI_TX_PARSE_STATE_CMD_COMPARE:

    							 if(ucFlagWaitResponseBalast == USER_TRUE)
    							 {
    								 ucFlagWaitResponseBalast = USER_FALSE;
    								 if(ucFlagCompareAddrAutoState == DALI_COMPARE_ADDR_AUTO_STATE_RX_OK)
    								 {
    									 ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_RX_OK;
    								 }
    								 else
    								 {
    									 ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_RX_NOK;
    								 }
    								 Pop((uint8_t*)&stcDaliTxFrame,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);
    						         memset((uint8_t*)&stcDaliTxFrame,0,DALI_TX_FIFO);
    								 enDaliTxFsmState = DALI_TX_FSM_WAIT_FIFO;
    							 }
    							 else
    							 {
    								 Pop((uint8_t*)&stcDaliTxFrame,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);
    						         memset((uint8_t*)&stcDaliTxFrame,0,DALI_TX_FIFO);
    								 enDaliTxFsmState = DALI_TX_FSM_WAIT_FIFO;
    							 }
        					break;
        					default:;
        				}
        			}
        		}
        	}

        break;
        default:
        	enDaliTxFsmState = DALI_TX_FSM_INIT;
        break;
    }
}

/******************************************************************************/
/*!\brief DaliFsmRxDispatcherTask
 * \author Jimmy ZANOTTO + Alejandro MEJIA
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
#ifdef USE_SCHEDULER
void DaliFsmRxDispatcherTask(en_change_behavior_fnt_t en_change_behavior_fnt)
#else
void DaliFsmRxDispatcherTask(void)
#endif
{
    static TICK uiLastTick_s;
    TICK uiCurrentTick_s;
    uint16_t usBytesInFifo = 0;

    stcDaliGenericRxFrame_t stcTmpDaliGenericRxFrame = {0};
    stcRuntimeDaliGateway_t stcRuntimeDaliGateway = {0};

#ifdef DALI_MASTER
    stcSPIArcomFrame_t stcSpiArcomFrame = {0};
#endif

    switch(enDaliRxDispatcherState)
    {
        case DALI_RX_DISP_STATE_INIT :
        	if(ucLineReady_g == DALI_LINE_OK)
        	{
            	enDaliRxDispatcherState = DALI_RX_DISP_STATE_WAIT_FIFO;
        	}
        break;
        case DALI_RX_DISP_STATE_WAIT_FIFO :

        	 usBytesInFifo = GetFifoNbBytes(DALI_RX_FIFO);
        	 if((usBytesInFifo < SIZE_DALI_RX_FRAME) &&
        			 (usBytesInFifo > 0))
        	 {
#ifdef DEBUG_DALI
        		dbgprint("%s -> Error frame size in fifo not ok\r\n",__FUNCTION__);
#endif
        	 }
        	 else
        	 {
        		 if(usBytesInFifo)
        		 {
#ifdef DEBUG_DALI
					 dbgprint("%s -> Item in  fifo found\r\n",__FUNCTION__);
#endif
					 enDaliRxDispatcherState = DALI_RX_DISP_STATE_PARSE_DATA;
        		 }
        	 }

        break;
        case DALI_RX_DISP_STATE_PARSE_DATA :

        	Pop((uint8_t*)&stcTmpDaliGenericRxFrame,SIZE_DALI_RX_FRAME,DALI_RX_FIFO);

        	if(stcTmpDaliGenericRxFrame.ucCodeTypeRsp == DALI_TYPE_RSP_BALLAST)
        	{
        		ucFlagCompareAddrAutoState = DALI_COMPARE_ADDR_AUTO_STATE_RX_OK;
        		ucValueFeedBackBallast = stcTmpDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr1;

				stcRuntimeDaliGateway.ucDaliGatewayType = DALI_GATEWAY_TYPE_BALLAST;
				stcRuntimeDaliGateway.ucDaliGatewayDtr1 = stcTmpDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr1;

#ifdef DALI_MASTER
	            stcSpiArcomFrame.ucCode = RUNTIME_DALI_GATEWAY;
	            memcpyST((uint8_t*)stcSpiArcomFrame.uData,(const uint8_t *)&stcRuntimeDaliGateway,sizeof(stcRuntimeDaliGateway_t));
	            AddCrc16ToFrame((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE);
	            Push((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE,SPI_TX_FIFO);
#endif

        	}
        	else if(stcTmpDaliGenericRxFrame.ucCodeTypeRsp == DALI_TYPE_RSP_ACC)
        	{
        		switch(stcTmpDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr1)
        		{
#if(0) //TODO JBL DALI
					case CMD_DALI_OCCUPATION :
					case CMD_DALI_TEMP_MEASURE :
					case CMD_DALI_FAN_SPEED :
					case CMD_DALI_PRESENCE :
					case CMD_DALI_LUMINOSITY :
					case CMD_DALI_LIGHT_UP :
					case CMD_DALI_LIGHT_DOWN :
					case CMD_DALI_BLIND_UP :
					case CMD_DALI_BLIND_DOWN :
					case CMD_DALI_TEMP_OFFSET :
					case CMD_DALI_HYGRO :
					case CMD_DALI_CO2 :
					case CMD_DALI_VERSION_ACC:
					case CMD_DALI_CONFIG_BALLAST:
					case CMD_DALI_ETAT_TEST_DALI:
					case CMD_DALI_TEST_GROUPE_LAMPE_DALI:
					case CMD_DALI_TEST_BALLAST_2:
					case CMD_DALI_CONFIG_BALLAST_2:
					case CMD_AJOUT_BALLAST_GROUPE:
					case CMD_SUPP_BALLAST_GROUPE:
						SetRoomControlRuntimeRsp(&stcTmpDaliGenericRxFrame);
					break;
					default:;
#endif
        		}
				stcRuntimeDaliGateway.ucDaliGatewayType = DALI_GATEWAY_TYPE_ACC;
				stcRuntimeDaliGateway.ucDaliGatewayDtr1 = stcTmpDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr1;
				stcRuntimeDaliGateway.ucDaliGatewayDtr0 = stcTmpDaliGenericRxFrame.stcDaliAccRxFrame.ucDtr;
				stcRuntimeDaliGateway.ucDaliGatewayAddr = stcTmpDaliGenericRxFrame.stcDaliAccRxFrame.ucAddrDest;
				stcRuntimeDaliGateway.ucDaliGatewayLsb = stcTmpDaliGenericRxFrame.stcDaliAccRxFrame.ucLsb;
				stcRuntimeDaliGateway.ucDaliGatewayMsb = stcTmpDaliGenericRxFrame.stcDaliAccRxFrame.ucMsb;

#ifdef DALI_MASTER
	            stcSpiArcomFrame.ucCode = RUNTIME_DALI_GATEWAY;
	            memcpyST((uint8_t*)stcSpiArcomFrame.uData,(const uint8_t *)&stcRuntimeDaliGateway,sizeof(stcRuntimeDaliGateway_t));
	            AddCrc16ToFrame((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE);
	            Push((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE,SPI_TX_FIFO);
#endif

        	}

        	enDaliRxDispatcherState = DALI_RX_DISP_STATE_WAIT_FIFO;

        break;

        default:
        	enDaliRxDispatcherState = DALI_RX_DISP_STATE_INIT;
        break;
    }
}
/******************************************************************************/
/*!\brief ParseDataDaliTx
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void ParseDataDaliTx(void)
{
	enDaliTxFrameType = DALI_TX_PARSE_STATE_INTER_FRAME;

	if(ucCurrentFrameTx == 5)// c�d octet 4
	{
		if((stcDaliTxFrame.ucDtr1 == 0) || (stcDaliTxFrame.ucDtr1 == 1))
		{
			if(stcDaliTxFrame.ucDtr1 == 1)
			{
				if(stcDaliTxFrame.ucLsb == 1)	//V43
				{
					ucFlagWaitResponseBalast = USER_TRUE;
				}
				ucTxDaliAdr = stcDaliTxFrame.ucDtr;
				ucTxDaliCmd = stcDaliTxFrame.ucCmd;
			}
			else
			{
				ucTxDaliAdr = stcDaliTxFrame.ucCmd;
				ucTxDaliCmd = stcDaliTxFrame.ucDtr;
			}

			ucCurrentFrameTx = 0;
			enDaliTxFrameType = DALI_TX_PARSE_STATE_CMD_BALLAST;
		}
		else if (stcDaliTxFrame.ucDtr1==2)
		{
			ucTxDaliAdr = CMD_DALI_SEARCHADDRH;
			ucTxDaliCmd = stcDaliTxFrame.ucDtr;
			ucCurrentFrameTx--;
		}
		else
		{
			ucTxDaliAdr = CMD_DALI_STORE_IN_DTR1;
			ucTxDaliCmd = stcDaliTxFrame.ucDtr1;
			ucCurrentFrameTx--;
		}
	}
	else if (ucCurrentFrameTx == 4) // c�d octet3
	{
		if (stcDaliTxFrame.ucDtr1 == 2)
		{
			ucTxDaliAdr = CMD_DALI_SEARCHADDRM;
			ucTxDaliCmd = stcDaliTxFrame.ucCmd;
			ucCurrentFrameTx--;
		}
		else
		{
			ucTxDaliAdr = CMD_DALI_STORE_IN_DTR;
			ucTxDaliCmd = stcDaliTxFrame.ucDtr;
			ucCurrentFrameTx--;
		}
	}
	else if((ucCurrentFrameTx == 3 ) && (stcDaliTxFrame.ucDtr1 == 2))
	{
		ucTxDaliAdr = CMD_DALI_SEARCHADDRL;
		ucTxDaliCmd = stcDaliTxFrame.ucLsb;
		ucCurrentFrameTx--;
	}
	else if((ucCurrentFrameTx == 3) 					&&
			(stcDaliTxFrame.ucCmd >= CMD_FIRST_CFG_CMD) &&
			(stcDaliTxFrame.ucCmd <= CMD_LAST_CFG_CMD) /*&& (compteur_repeat<=1)*/)
	{
		ucTxDaliAdr = stcDaliTxFrame.ucAdresse;
		ucTxDaliAdr = (ucTxDaliAdr << 1) + 1;
		ucTxDaliCmd = stcDaliTxFrame.ucCmd;
		ucCurrentFrameTx--;
	}
	else if (ucCurrentFrameTx == 2)  // c�d octet 1
	{
		if (stcDaliTxFrame.ucDtr1 == 2)
		{
			ucCurrentFrameTx=0;

			ucFlagWaitResponseBalast = USER_TRUE;

			ucTxDaliAdr = CMD_DALI_COMPARE;
			ucTxDaliCmd = 0;
			enDaliTxFrameType = DALI_TX_PARSE_STATE_CMD_COMPARE;
		}
		else
		{
			ucTxDaliAdr = CMD_DALI_WRITE_MEMORY;
			ucTxDaliCmd = stcDaliTxFrame.ucLsb;
			ucCurrentFrameTx--;
		}
	}
	else if (ucCurrentFrameTx == 1) // c�d octet 0
	{
		ucTxDaliAdr = CMD_DALI_WRITE_MEMORY;
		ucTxDaliCmd = stcDaliTxFrame.ucMsb;
		ucCurrentFrameTx--;
		enDaliTxFrameType = DALI_TX_PARSE_STATE_END;
	}
}

void BuildFrameDaliTx(void)
{
	if(ucNbCmdFrameRep==0)
	{
		ucNbCmdFrameRep=1;

		if((ucTxDaliAdr & 0x01)==1)
		{/*(ucTxDaliAdr & 0x01)==1 ----> CMD ; (ucTxDaliAdr & 0x01)==0 ----> Variation*/
			if(((ucTxDaliAdr & 0xE0) != 0xA0 ) && ((ucTxDaliAdr & 0xE0) != 0xC0))
			{
				if((ucTxDaliCmd >= CMD_FIRST_CFG_CMD) &&
				  (ucTxDaliCmd <= CMD_LAST_CFG_CMD))/* Si commande dali entre ces 2 valeur --> l'emettre 2 fois*/
				{
					ucNbCmdFrameRep=2;
				}

			}
			if((ucTxDaliAdr == 0x0A5) ||
					(ucTxDaliAdr == 0x0A7) ||
					((ucTxDaliAdr == 0xFF) && (ucTxDaliCmd == 0x20)) ||
					((ucTxDaliAdr == ADRESSE_BROADCAST_DALI_CMD) && (ucTxDaliCmd == CMD_DALI_227)))
			{
				ucNbCmdFrameRep=2;
			}
		}
	}

	ucNbCmdFrameRep--;

	unFrameDali.Request.Start = 1;
	unFrameDali.Request.AdresseMsb  = (ucTxDaliAdr & 0xFE)>>1;
	unFrameDali.Request.AdresseLsb  = (ucTxDaliAdr & 0x01);
	unFrameDali.Request.CommandeMsb = (ucTxDaliCmd & 0xFE)>>1;
	unFrameDali.Request.CommandeLsb = (ucTxDaliCmd & 0x01);
    ucFrameDaliBuffer_pt = unFrameDali.ucBuffer;

	uiTransmitBits = ucTxDaliAdr;
	uiTransmitBits = uiTransmitBits << 8;
	uiTransmitBits = uiTransmitBits + ucTxDaliCmd;

	dbgprint("%s -> uiTransmitBits : %d\r\n",__FUNCTION__,uiTransmitBits);

}


//void CallBackTimerDaliTx(TIM_HandleTypeDef *htim)
void CallBackTimerDaliTx(void)
{
    if(ucMaskBitFrameTxDali != 0)
    {
		// �mission d'un bit
		// Emission d'un 1 1er niveau ou d'un 0 2eme niveau:
		if((((*ucFrameDaliBuffer_pt & ucMaskBitFrameTxDali) == 0) && (ucFlagStateManchester == 1)) ||
		   (((*ucFrameDaliBuffer_pt & ucMaskBitFrameTxDali) != 0) && (ucFlagStateManchester == 0)))
		{
			TX_DALI_LOW();
		}
		// Emission d'un 0 1er niveau ou d'un 1 2eme niveau:
		if((((*ucFrameDaliBuffer_pt & ucMaskBitFrameTxDali) == 0) && (ucFlagStateManchester == 0)) ||
		   (((*ucFrameDaliBuffer_pt & ucMaskBitFrameTxDali) != 0) && (ucFlagStateManchester == 1)))
		{
			TX_DALI_HIGH();
		}
		// si on vient d'emmetre le 2eme niveau, on pr�pare l'�criture suivante
		if(ucFlagStateManchester==1)
		{
			ucCntBitsTxDali++;
			//Test si on est pas en fin de trame:
			if(ucCntBitsTxDali != ucTxDaliBitsSize)
			{
				// d�calage du bit
    	    	if((ucMaskBitFrameTxDali >>= 1) == 0)
        		{
            	    // lancement octet suivant
                	ucFrameDaliBuffer_pt++;
                	ucMaskBitFrameTxDali = 0x80;
            	}
			}
			else//Fin de trame Dali
			{
				ucMaskBitFrameTxDali = 0;
			}

		}
		// Passage du niveau 1 au 2 et inversement
		ucFlagStateManchester^=1;
    }
    else
    {
    	TX_DALI_HIGH();
		if (HAL_TIM_Base_Stop_IT(&timDaliFrontTransitionTx) != HAL_OK)
		{
			/* Starting Error */
			Error_Handler();
		}
		__HAL_TIM_SET_COUNTER(&timDaliFrontTransitionTx,0);
    }
}

void CallBackTimInterPacketTx(void)
{
	ucFlagTimInterPacketTx = USER_TRUE;
}

void CallBackTimInterFrameTx(void)
{
	ucFlagTimInterFrameTx = USER_TRUE;
}


int8_t SetFrameTx(uint8_t ucMsb, uint8_t ucLsb, uint8_t ucCmd, uint8_t ucDtr, uint8_t ucDtr1, uint8_t ucAdresse)
{
	int8_t cRet = 0;
	stcDaliTxFrame_t stcDaliTxFrame = {0};

	stcDaliTxFrame.ucDtr1 = ucDtr1;
	stcDaliTxFrame.ucDtr = ucDtr;
	stcDaliTxFrame.ucCmd = ucCmd;
	stcDaliTxFrame.ucLsb = ucLsb;
	stcDaliTxFrame.ucMsb = ucMsb;
	stcDaliTxFrame.ucAdresse = ucAdresse;

	cRet = Push((uint8_t *)&stcDaliTxFrame,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);

    return cRet;
}

int8_t SimpleSetFrameTx(uint8_t ucCodeDali)
{
	int8_t cRet = 0;

	switch(ucCodeDali)
	{
		case CMD_DALI_SET_OFF :
			cRet = SetFrameTx(0,0,CMD_DALI_SET_OFF,0xFF,1,0);
		break;
		case CMD_DALI_BLOCAGE :
			cRet = SetFrameTx(0xC0+(ADDR_DALI_MASTER & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,ADDR_DALI_MASTER);
		break;
		case CMD_DALI_RESET :
			cRet = SetFrameTx(0,0,CMD_DALI_RESET,0xFF,1,0);
		break;
		case CMD_DALI_INITIALIZE :
			cRet = SetFrameTx(0,0,0,CMD_DALI_INITIALIZE,1,0);
		break;
		case CMD_DALI_TERMINATE :
			cRet = SetFrameTx(0,0,0,CMD_DALI_TERMINATE,1,0);
		break;
		case CMD_DALI_STORE_IN_DTR :
			cRet = SetFrameTx(0,0,0xFF,CMD_DALI_STORE_IN_DTR,1,0);
		break;
		case CMD_DALI_STORE_DTR_AS_SHORT_ADR :
			cRet = SetFrameTx(0,0,CMD_DALI_STORE_DTR_AS_SHORT_ADR,0xFF,1,0);
		break;
		case CMD_DALI_RANDOMIZE :
			cRet = SetFrameTx(0,0,0,CMD_DALI_RANDOMIZE,1,0);
		break;
		case CMD_DALI_COMPARE :
			cRet = SetFrameTx(0,1,0,CMD_DALI_COMPARE,1,0);
		break;
		case CMD_DALI_WITHDRAW :
			cRet = SetFrameTx(0,0,0,CMD_DALI_WITHDRAW,1,0);
		break;
		case CMD_DALI_RECALL_MAX_LEVEL:
			cRet = SetFrameTx(0,0,CMD_DALI_RECALL_MAX_LEVEL,0xFF,1,0);
		break;
		default:cRet = -1;
	}

	return cRet;
}


/******************************************************************************/
/*!\brief GetConfigDaliBallast
 * \author Jimmy ZANOTTO
 *
 * \param
 *
 * \return      uint8_t.
 *
 * long description :
 *
 ******************************************************************************/
uint8_t GetConfigDaliBallast(void)
{
	return ucFlagWaitStartAddrAuto;
}
/******************************************************************************/
/*!\brief DaliFsmAdrAutoTask
 * \author Jimmy ZANOTTO + RFR
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void DaliFsmReplTask(void)
{
	//ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_STANDBY;
	//ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
	//ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
	//ucAddrAutoDali=USER_FALSE;
	//ucValueFeedBackBallast=0;
	//SetFrameTx(0,1,CMD_DALI_QUERY_ACTUAL_LVL,0xff,1,0);


    static TICK uiLastTick_s;
    TICK uiCurrentTick_s;

	static uint16_t usProgramingShortAddr = 0;
	static uint8_t ucAddrBallastLibre = 0;
	static uint8_t ucReplaceAdd = 0;
	static uint64_t ulAddrBalastBusy = 0;
	static unuiSearchAddress_t ulSearchAddrMax = {0};
	static unuiSearchAddress_t ulSearchAddrMin = {0};
	static unuiSearchAddress_t ulSearchAddr = {0};
	static uint8_t ucCompteurNB = 0;

    if(enDaliFsmReplState != DALI_FSM_REPL_INIT)
    {
    	uiCurrentTick_s = tickGet();

    	if(uiLastTick_s > uiCurrentTick_s)
    	{/*protection*/
    		uiLastTick_s = uiCurrentTick_s;
    	}

    	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= uiDefaultAddrAutoAndRemplTimeOut)
    	{
    		enDaliFsmReplState = DALI_FSM_REPL_TERMINATE;
    	}
    }
    switch(enDaliFsmReplState)
    {
    	case DALI_FSM_REPL_INIT :
    		uiLastTick_s = tickGet();
        	usProgramingShortAddr = 0;
        	ucCompteurNB = 0;
        	ulAddrBalastBusy=0;
        	ucReplaceAdd=0;
        	ucValueFeedBackBallast=0;
        	ucAddrBallastLibre=0;
        	enDaliFsmReplState=DALI_FSM_INV_OFF;
    	break;

    	case DALI_FSM_REPL_OFF :
         	SetFrameTx(0,0,CMD_DALI_SET_OFF,0xFF,1,0);
         	//SimpleSetFrameTx(CMD_DALI_SET_OFF);
         	enDaliFsmReplState=DALI_FSM_INV_LOCK_ACC;
    	break;

    	case DALI_FSM_REPL_LOCK_ACC :
        	SetFrameTx(0xC0+(ADDR_DALI_MASTER & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,63);
        	//SetFrameTx(0xC0+(NumeroAppareilReglage & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,63);
        	enDaliFsmReplState=DALI_FSM_REPL_SEARCH;
    	break;

    	case DALI_FSM_REPL_SEARCH :
        	if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
        	{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,145,(ucAddrBallastLibre<<1)+1,1,0);
				enDaliFsmReplState=DALI_FSM_REPL_FIND;
        	}
    	break;

    	case DALI_FSM_REPL_FIND :
        	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
        	{
        		if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
        		{
        			ulAddrBalastBusy+=(1<<ucAddrBallastLibre);
        		}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{
					if(ucReplaceAdd == 0)
					{
						if(ucAddrBallastLibre != 0)
						{
							ucReplaceAdd = (ucAddrBallastLibre<<1)+1;
						}
					}
        		}
        		ucAddrBallastLibre++;
				if(ucAddrBallastLibre == 33)
				{
					enDaliFsmReplState = DALI_FSM_REPL_RESET;
				}
				else
				{
					enDaliFsmReplState = DALI_FSM_REPL_SEARCH;
				}
        	}
    	break;

    	case DALI_FSM_REPL_RESET :
        	//SetFrameTx(0,0,CMD_DALI_RESET,0xFF,1,0);
        	//SetFrameTx(0,0,CMD_DALI_RESET,0xFF,1,0);
        	//SetFrameTx(0,0,CMD_DALI_RESET,0xFF,1,0);
        	//SetFrameTx(0,0,CMD_DALI_RESET,0xFF,1,0);
        	enDaliFsmReplState = DALI_FSM_REPL_INITIALIZE;
    	break;

    	case DALI_FSM_REPL_INITIALIZE :
        	SetFrameTx(0,0,0,CMD_DALI_INITIALIZE,1,0);
        	SetFrameTx(0,0,0,CMD_DALI_TERMINATE,1,0);
        	SetFrameTx(0,0,0,CMD_DALI_INITIALIZE,1,0);
        	//SetFrameTx(0,0,0,CMD_DALI_INITIALIZE,1,0);
        	//SetFrameTx(0,0,0,CMD_DALI_TERMINATE,1,0);
        	//SetFrameTx(0,0,0,CMD_DALI_INITIALIZE,1,0);
        	enDaliFsmReplState = DALI_FSM_REPL_RANDOMIZE;
    	break;

    	case DALI_FSM_REPL_RANDOMIZE :
        	SetFrameTx(0,0,0,CMD_DALI_RANDOMIZE,1,0);
        	enDaliFsmReplState = DALI_FSM_REPL_INIT_BISECTION;
    	break;

    	case DALI_FSM_REPL_INIT_BISECTION :
			ulSearchAddrMax.uiBuffer = 0x00FFFFFF;
			ulSearchAddrMin.uiBuffer = 0;
			ulSearchAddr.uiBuffer = (ulSearchAddrMin.uiBuffer + ulSearchAddrMax.uiBuffer)/2;
			SetFrameTx(0,ulSearchAddr.stcOctet.ucAddrL,ulSearchAddr.stcOctet.ucAddrM,ulSearchAddr.stcOctet.ucAddrH,2,0);
			enDaliFsmReplState = DALI_FSM_REPL_COMPARE;
    	break;

    	case DALI_FSM_REPL_COMPARE :
        	if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
        	{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,0,CMD_DALI_COMPARE,1,0);
				enDaliFsmReplState = DALI_FSM_REPL_RESPONSE_BALLAST_COMPARE;
        	}
    	break;

    	case DALI_FSM_REPL_RESPONSE_BALLAST_COMPARE :
        	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
        	{
        		if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
        		{
					if(ulSearchAddrMax.uiBuffer ==  ulSearchAddrMin.uiBuffer )
					{
						enDaliFsmReplState = DALI_FSM_REPL_QUERY_SHORT;
					}
					else // Replace les bornes de la dichotomie en fct de la r�ponse:
					{
						ulSearchAddrMax.uiBuffer = ulSearchAddr.uiBuffer;
						//cas normal de la dicotomie recalcul l'addresse � demander:
						ulSearchAddr.uiBuffer = (ulSearchAddrMin.uiBuffer + ulSearchAddrMax.uiBuffer)/2;
						SetFrameTx(0,ulSearchAddr.stcOctet.ucAddrL,ulSearchAddr.stcOctet.ucAddrM,ulSearchAddr.stcOctet.ucAddrH,2,0);
						enDaliFsmReplState = DALI_FSM_REPL_COMPARE;
					}
        		}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{
        			if ((ulSearchAddrMax.uiBuffer == 0x00FFFFFF)&&(ulSearchAddrMax.uiBuffer ==  ulSearchAddrMin.uiBuffer ))
        			{
        			// cas aucun ballast � programmer : continu sur terminate:
        				enDaliFsmReplState = DALI_FSM_REPL_TERMINATE;
        			}
        			else if(ulSearchAddrMax.uiBuffer ==  ulSearchAddrMin.uiBuffer )	//cas ou un ballast a repondu yes puis a �t� deconecter
        			{
        				enDaliFsmReplState = DALI_FSM_REPL_INIT_BISECTION;
        			}
        			else
        			{
        				ulSearchAddrMin.uiBuffer = ulSearchAddr.uiBuffer + 1;			//pas de reponse on change les bornes
        				//cas normal de la dicotomie recalcul l'addresse � demander:
        				ulSearchAddr.uiBuffer = (ulSearchAddrMin.uiBuffer + ulSearchAddrMax.uiBuffer)/2;
						SetFrameTx(0,ulSearchAddr.stcOctet.ucAddrL,ulSearchAddr.stcOctet.ucAddrM,ulSearchAddr.stcOctet.ucAddrH,2,0);
						enDaliFsmReplState = DALI_FSM_REPL_COMPARE;
        			}
        		}
        	}
    	break;

    	case DALI_FSM_REPL_QUERY_SHORT :
        	if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
        	{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_FALSE;
				ucValueFeedBackBallast=0;
				SetFrameTx(0,1,0,CMD_DALI_QUERY_SHORT_ADDRESS,1,0);
				enDaliFsmReplState = DALI_FSM_REPL_RESPONSE_QUERY_SHORT;
        	}
    	break;

    	case DALI_FSM_REPL_RESPONSE_QUERY_SHORT :
           	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
			{
           		ucTestIncTab = ucValueFeedBackBallast;
				if ((ucValueFeedBackBallast>1)&&(ucValueFeedBackBallast<66))	//reponse yes du ballast
				{

					ucCompteurNB=0;
					enDaliFsmReplState=DALI_FSM_REPL_WITHDRAW;
					if(ucValueFeedBackBallast == ucReplaceAdd)
					{
						enDaliFsmReplState=DALI_FSM_REPL_TERMINATE;
					}
				}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{

					if(ucCompteurNB <= 6)
					{
						enDaliFsmReplState = DALI_FSM_REPL_QUERY_SHORT;
						ucCompteurNB++;
					}
					else
					{
						enDaliFsmReplState = DALI_FSM_REPL_INIT_BISECTION;
						ucCompteurNB = 0;
					}
        		}
        		else
        		{
        			enDaliFsmReplState = DALI_FSM_REPL_PROGRAM_SHORT_ADDR;
        		}
			}
    	break;

    	case DALI_FSM_REPL_PROGRAM_SHORT_ADDR :
        	SetFrameTx(0,0,ucReplaceAdd,CMD_DALI_PROGRAM_SHORT_ADDRESS,1,0);
        	enDaliFsmReplState = DALI_FSM_REPL_VERIFY_SHORT_ADDR;
    	break;

    	case DALI_FSM_REPL_VERIFY_SHORT_ADDR :
        	if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
        	{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,ucReplaceAdd,CMD_DALI_VERIFY_SHORT_ADDRESS,1,0);
				enDaliFsmReplState = DALI_FSM_REPL_RESPONSE_BALLAST_SHORT_ADDR;
        	}
    	break;

    	case DALI_FSM_REPL_RESPONSE_BALLAST_SHORT_ADDR :
        	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
        	{
        		if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
        		{
        			ucCompteurNB=0;
        			enDaliFsmReplState = DALI_FSM_REPL_TERMINATE;
        		}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{

					if(ucCompteurNB<=6)
					{
						enDaliFsmReplState = DALI_FSM_REPL_PROGRAM_SHORT_ADDR;
						ucCompteurNB++;
					}
					else
					{
						enDaliFsmReplState = DALI_FSM_REPL_INIT_BISECTION;
						ucCompteurNB = 0;
					}
        		}
        	}
    	break;

    	case DALI_FSM_REPL_WITHDRAW :
        	SetFrameTx(0,0,0,CMD_DALI_WITHDRAW,1,0);
        	enDaliFsmReplState = DALI_FSM_REPL_COMPAREW;
    	break;

    	case DALI_FSM_REPL_COMPAREW :
        	if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
        	{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,0,CMD_DALI_COMPARE,1,0);
				enDaliFsmReplState = DALI_FSM_REPL_RESPONSE_BALLAST_WITHDRAW;
        	}
    	break;

    	case DALI_FSM_REPL_RESPONSE_BALLAST_WITHDRAW :
           	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
			{
				if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
				{
					if(ucCompteurNB<=6)
					{
						enDaliFsmReplState = DALI_FSM_REPL_WITHDRAW;
						ucCompteurNB++;
					}
					else
					{
						ucCompteurNB = 0;
						enDaliFsmReplState = DALI_FSM_REPL_INIT_BISECTION;
					}
				}
				else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
				{
					usProgramingShortAddr++;
					enDaliFsmReplState = DALI_FSM_REPL_INIT_BISECTION;
					//Si ts les ballasts sont programm�s : continu sur TERMINATE
					if(usProgramingShortAddr >= 33)
					{
						enDaliFsmReplState = DALI_FSM_REPL_TERMINATE;
					}
				}
			}

    	break;

    	case DALI_FSM_REPL_TERMINATE :
			SetFrameTx(0,0,0,CMD_DALI_TERMINATE,1,0);
			SetFrameTx((ADDR_DALI_MASTER & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,63);
			SetFrameTx(0,0,CMD_DALI_SET_OFF,0xFF,1,0);
			enDaliFsmReplState = DALI_FSM_REPL_INIT;
			ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_STANDBY;
    	break;

		default:
			enDaliFsmReplState = DALI_FSM_REPL_INIT;
		break;
    }

}

/******************************************************************************/
/*!\brief DaliFsmAdrAutoTask
 * \author Jimmy ZANOTTO + RFR
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void DaliFsmAdrAutoTask(void)
{

    static TICK uiLastTick_s;
    static TICK uiTickStandby_s;
    TICK uiCurrentTick_s;

	static uint16_t usProgramingShortAddr = 0;
	static unuiSearchAddress_t ulSearchAddrMax = {0};
	static unuiSearchAddress_t ulSearchAddrMin = {0};
	static unuiSearchAddress_t ulSearchAddr = {0};
	static uint8_t ucCompteurNB = 0;

    uint16_t usGroupDALI=0;


    if(enDaliFsmAdrState != DALI_FSM_ADR_AUTO_INIT)
    {
    	uiCurrentTick_s = tickGet();

    	if(uiLastTick_s > uiCurrentTick_s)
    	{/*protection*/
    		uiLastTick_s = uiCurrentTick_s;
    	}

    	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= uiDefaultAddrAutoAndRemplTimeOut)
    	{
    		enDaliFsmAdrState = DALI_FSM_ADR_AUTO_TERMINATE;
    	}
    }

    switch(enDaliFsmAdrState)
    {
        case DALI_FSM_ADR_AUTO_INIT :

        	uiLastTick_s = tickGet();
        	usProgramingShortAddr=1;
        	ucCompteurNB = 0;
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_OFF;
        break;
         case DALI_FSM_ADR_AUTO_OFF :
        	 SetFrameTx(0,0,CMD_DALI_SET_OFF,0xFF,1,0);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_LOCK_ACC;
        break;
        case DALI_FSM_ADR_AUTO_LOCK_ACC :
        	SetFrameTx(0xC0+(ADDR_DALI_MASTER & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,63);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_REMOVE_GROUPS;
        break;
        case DALI_FSM_ADR_AUTO_REMOVE_GROUPS :
        	for(usGroupDALI=0; usGroupDALI<16; usGroupDALI++)
        	{
        		SetFrameTx(0,0,CMD_DALI_REMOVE_FROM_GROUP0+usGroupDALI,0xFF,1,0);
        	}
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_RESET;
        break;
        case DALI_FSM_ADR_AUTO_RESET :
        	SetFrameTx(0,0,CMD_DALI_RESET,0xFF,1,0);
        	SetFrameTx(0,0,CMD_DALI_RESET,0xFF,1,0);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INITIALIZE;
        break;
        case DALI_FSM_ADR_AUTO_INITIALIZE :
        	SetFrameTx(0,0,0,CMD_DALI_INITIALIZE,1,0);
        	SetFrameTx(0,0,0,CMD_DALI_TERMINATE,1,0);
        	SetFrameTx(0,0,0,CMD_DALI_INITIALIZE,1,0);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_ADR_FACTORY;
        break;
        case DALI_FSM_ADR_AUTO_ADR_FACTORY :
        	SetFrameTx(0,0,0xFF,0xA3,1,0);
        	SetFrameTx(0,0,0x80,0xFF,1,0);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_RANDOMIZE;
        break;
        case DALI_FSM_ADR_AUTO_RANDOMIZE :
        	SetFrameTx(0,0,0,CMD_DALI_RANDOMIZE,1,0);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INIT_BISECTION;
        break;
        case DALI_FSM_ADR_AUTO_INIT_BISECTION :
			ulSearchAddrMax.uiBuffer = 0x00FFFFFF;
			ulSearchAddrMin.uiBuffer = 0;
			ulSearchAddr.uiBuffer = (ulSearchAddrMin.uiBuffer+ ulSearchAddrMax.uiBuffer)/2;
			SetFrameTx(0,ulSearchAddr.stcOctet.ucAddrL,ulSearchAddr.stcOctet.ucAddrM,ulSearchAddr.stcOctet.ucAddrH,2,0);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_COMPARE;
        break;
        case DALI_FSM_ADR_AUTO_COMPARE :
        	if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
        	{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,0,CMD_DALI_COMPARE,1,0);
				enDaliFsmAdrState = DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_COMPARE;
        	}
        break;
        case DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_COMPARE :
        	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
        	{
        		if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
        		{
					if(ulSearchAddrMax.uiBuffer ==  ulSearchAddrMin.uiBuffer )
					{
						enDaliFsmAdrState = DALI_FSM_ADR_AUTO_PROGRAM_SHORT_ADDR;
					}
					else // Replace les bornes de la dichotomie en fct de la r�ponse:
					{
						ulSearchAddrMax.uiBuffer = ulSearchAddr.uiBuffer;
						//cas normal de la dicotomie recalcul l'addresse � demander:
						ulSearchAddr.uiBuffer = (ulSearchAddrMin.uiBuffer + ulSearchAddrMax.uiBuffer)/2;
						SetFrameTx(0,ulSearchAddr.stcOctet.ucAddrL,ulSearchAddr.stcOctet.ucAddrM,ulSearchAddr.stcOctet.ucAddrH,2,0);
						enDaliFsmAdrState = DALI_FSM_ADR_AUTO_COMPARE;
					}
        		}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{
        			if ((ulSearchAddrMax.uiBuffer == 0x00FFFFFF)&&(ulSearchAddrMax.uiBuffer ==  ulSearchAddrMin.uiBuffer ))
        			{
        			// cas aucun ballast � programmer : continu sur terminate:
        				enDaliFsmAdrState = DALI_FSM_ADR_AUTO_TERMINATE;
        			}
        			else if(ulSearchAddrMax.uiBuffer ==  ulSearchAddrMin.uiBuffer )	//cas ou un ballast a repondu yes puis a �t� deconecter
        			{
        				enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INIT_BISECTION;
        			}
        			else
        			{
        				ulSearchAddrMin.uiBuffer = ulSearchAddr.uiBuffer + 1;			//pas de reponse on change les bornes
        				//cas normal de la dicotomie recalcul l'addresse � demander:
        				ulSearchAddr.uiBuffer = (ulSearchAddrMin.uiBuffer + ulSearchAddrMax.uiBuffer)/2;
						SetFrameTx(0,ulSearchAddr.stcOctet.ucAddrL,ulSearchAddr.stcOctet.ucAddrM,ulSearchAddr.stcOctet.ucAddrH,2,0);
						enDaliFsmAdrState = DALI_FSM_ADR_AUTO_COMPARE;
        			}
        		}
        	}
        break;

        case DALI_FSM_ADR_AUTO_PROGRAM_SHORT_ADDR :
        	SetFrameTx(0,0,(((usProgramingShortAddr&0x7F)<<1)|0x01),CMD_DALI_PROGRAM_SHORT_ADDRESS,1,0);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_VERIFY_SHORT_ADDR;
        break;

        case DALI_FSM_ADR_AUTO_VERIFY_SHORT_ADDR :
        	if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
        	{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,(((usProgramingShortAddr&0x7F)<<1)|0x01),CMD_DALI_VERIFY_SHORT_ADDRESS,1,0);
				enDaliFsmAdrState = DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_SHORT_ADDR;
        	}
        break;

        case DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_SHORT_ADDR :
        	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
        	{
        		if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
        		{
        			ucCompteurNB=0;
        			enDaliFsmAdrState = DALI_FSM_ADR_AUTO_WITHDRAW;
        		}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{

					if(ucCompteurNB<=6)
					{
						enDaliFsmAdrState = DALI_FSM_ADR_AUTO_PROGRAM_SHORT_ADDR;
						ucCompteurNB++;
					}
					else
					{
						enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INIT_BISECTION;
					}
        		}
        	}

        break;

        case DALI_FSM_ADR_AUTO_WITHDRAW :
        	SetFrameTx(0,0,0,CMD_DALI_WITHDRAW,1,0);
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_COMPAREW;
        break;

        case DALI_FSM_ADR_AUTO_COMPAREW :
        	if(GetFifoNbBytes(DALI_TX_FIFO) == 0)
        	{
				ucFlagCompareAddrAuto = DALI_COMPARE_ADDR_AUTO_IN_PROGRESS;
				ucFlagCompareAddrAutoState=DALI_COMPARE_ADDR_AUTO_STATE_RX_NOK;
				ucAddrAutoDali=USER_TRUE;
				SetFrameTx(0,1,0,CMD_DALI_COMPARE,1,0);
				enDaliFsmAdrState = DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_WITHDRAW;
        	}
        break;

        case DALI_FSM_ADR_AUTO_RESPONSE_BALLAST_WITHDRAW :
        	if(ucFlagCompareAddrAuto!=DALI_COMPARE_ADDR_AUTO_IN_PROGRESS)
        	{
        		if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_OK)
        		{
					if(ucCompteurNB<=6)
					{
						enDaliFsmAdrState = DALI_FSM_ADR_AUTO_WITHDRAW;
						ucCompteurNB++;
					}
					else
					{
						enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INIT_BISECTION;
					}
        		}
        		else if(ucFlagCompareAddrAuto==DALI_COMPARE_ADDR_AUTO_RX_NOK)
        		{
					usProgramingShortAddr++;
					enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INIT_BISECTION;
					//Si ts les ballasts sont programm�s : continu sur TERMINATE
					if(usProgramingShortAddr >= 33)
					{
						enDaliFsmAdrState = DALI_FSM_ADR_AUTO_TERMINATE;
					}
        		}
        	}
        break;

        case DALI_FSM_ADR_AUTO_TERMINATE :
			SetFrameTx(0,0,0,CMD_DALI_TERMINATE,1,0);
			SetFrameTx(0,0,CMD_DALI_SET_OFF,0xFF,1,0);
			enDaliFsmAdrState = DALI_FSM_ADR_AUTO_STANDBY;

			uiTickStandby_s = uiCurrentTick_s;

			if(uiTickStandby_s > uiCurrentTick_s)
			{/*protection*/
				uiTickStandby_s = uiCurrentTick_s;
			}
        break;

        case DALI_FSM_ADR_AUTO_STANDBY:

			if(uiTickStandby_s > uiCurrentTick_s)
			{/*protection*/
				uiTickStandby_s = uiCurrentTick_s;
			}

			if(TICK_GET_DIFF(uiCurrentTick_s,uiTickStandby_s) >= (TICK_SECOND * 10) )
			{
				SetFrameTx((ADDR_DALI_MASTER & 0x3F),0xFF,CMD_DALI_ENABLE_WRITE_MEMORY,0,CMD_DALI_BLOCAGE,63);
				ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_STANDBY;
				enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INIT;
			}
        break;

        default:
        	enDaliFsmAdrState = DALI_FSM_ADR_AUTO_INIT;
        break;
    }

}


/******************************************************************************/
/*!\brief SetQueryDaliRuntime
 * \author Jimmy ZANOTTO
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetQueryDaliRuntime(void)
{
	if( (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_STANDBY)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_GROUPE_TYPE)
		|| (ucFlagWaitStartAddrAuto == DALI_BALLAST_CONF_TEST_BALLAST_TYPE) )
	{
		ucFlagWaitStartAddrAuto = DALI_BALLAST_CONF_QUERY_TYPE;
	}
}







/******************************************************************************/
/*
 * \author Jimmy ZANOTTO
 *
 * \param       uint8_t ucSelect
 *
 * \return      uint16_t.
 *
 * long description :
 *
 ******************************************************************************/
uint16_t GetConfigDali(uint8_t ucSelect)
{
	switch(ucSelect)
	{
		case CONFIG_DALI_BIT_STOP :
			return ucDefaultBitStopPeriod;
		break;
		case CONFIG_DALI_INTER_PACKET_RX :
			return ucDefaultInterPacketRxPeriod;
		break;
		case CONFIG_DALI_INTER_FRAME_TX :
			return ucDefaultInterFrameTxPeriod;
		break;
		case CONFIG_DALI_INTER_PACKET_TX :
			return ucDefaultInterPacketTxPeriod;
		break;
		case CONFIG_DALI_PERCENT_TOLERANCE :
			return ucDefaultPercentTolerance;
		break;
		case CONFIG_DALI_CORRECTION_OPTO :
			return ucDefaultCorrectionOpto;
		break;
		default:
			return 0;
	}
}

/******************************************************************************/
/*!\brief SetConfigDali
 * \author Jimmy ZANOTTO
 *
 * \param       stcSystemConfig_t *stcSystemConfig_pt
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetConfigDali(stcSystemConfig_t *stcSystemConfig_pt)
{
	uint8_t ucFlagTxSpi = 0;
	uint32_t uiTmp = 0;
#ifdef DALI_MASTER
	stcSPIArcomFrame_t stcSpiArcomFrame = {0};
#endif
	stcSystemConfig_t stcSystemConfig = {0};

	if(stcSystemConfig_pt->ucSystemConfigBitStop != 0)
	{
		if(ucDefaultBitStopPeriod != stcSystemConfig_pt->ucSystemConfigBitStop)
		{
			ucDefaultBitStopPeriod = stcSystemConfig_pt->ucSystemConfigBitStop;
			ucFlagTxSpi = 1;
		}
	}
	if(stcSystemConfig_pt->ucSystemConfigCorrectionOpto != 0)
	{
		if(ucDefaultCorrectionOpto != stcSystemConfig_pt->ucSystemConfigCorrectionOpto)
		{
			ucDefaultCorrectionOpto = stcSystemConfig_pt->ucSystemConfigCorrectionOpto;
			ucFlagTxSpi = 1;
		}
	}
	if(stcSystemConfig_pt->ucSystemConfigInterFrameTx != 0)
	{
		if(ucDefaultInterFrameTxPeriod != stcSystemConfig_pt->ucSystemConfigInterFrameTx)
		{
			ucDefaultInterFrameTxPeriod = stcSystemConfig_pt->ucSystemConfigInterFrameTx;
			ucFlagTxSpi = 1;
		}
	}
	if(stcSystemConfig_pt->ucSystemConfigInterPacketRx != 0)
	{
		if(ucDefaultInterPacketRxPeriod != stcSystemConfig_pt->ucSystemConfigInterPacketRx)
		{
			ucDefaultInterPacketRxPeriod = stcSystemConfig_pt->ucSystemConfigInterPacketRx;
			ucFlagTxSpi = 1;
		}
	}
	if(stcSystemConfig_pt->ucSystemConfigInterPacketTx != 0)
	{
		if(ucDefaultInterPacketTxPeriod != stcSystemConfig_pt->ucSystemConfigInterPacketTx)
		{
			ucDefaultInterPacketTxPeriod = stcSystemConfig_pt->ucSystemConfigInterPacketTx;
			ucFlagTxSpi = 1;
		}
	}
	if(stcSystemConfig_pt->ucSystemConfigPercentTolerance != 0)
	{
		if(ucDefaultPercentTolerance != stcSystemConfig_pt->ucSystemConfigPercentTolerance)
		{
			ucDefaultPercentTolerance = stcSystemConfig_pt->ucSystemConfigPercentTolerance;
			ucFlagTxSpi = 1;
		}
	}
	if((stcSystemConfig_pt->ucSystemConfigLsbAddrAutoTimeOut != 0) ||
			(stcSystemConfig_pt->ucSystemConfigMsbAddrAutoTimeOut != 0))
	{
		uiTmp = stcSystemConfig_pt->ucSystemConfigMsbAddrAutoTimeOut;
		uiTmp = uiTmp << 8;
		uiTmp = uiTmp + stcSystemConfig_pt->ucSystemConfigLsbAddrAutoTimeOut;
		uiTmp = uiTmp * 1000;
		if(uiDefaultAddrAutoAndRemplTimeOut != uiTmp)
		{
			uiDefaultAddrAutoAndRemplTimeOut = uiTmp;
			ucFlagTxSpi = 1;
		}
	}

	stcSystemConfig.ucSystemConfigBitStop = ucDefaultBitStopPeriod;
	stcSystemConfig.ucSystemConfigCorrectionOpto = ucDefaultCorrectionOpto;
	stcSystemConfig.ucSystemConfigInterFrameTx = ucDefaultInterFrameTxPeriod;
	stcSystemConfig.ucSystemConfigInterPacketRx = ucDefaultInterPacketRxPeriod;
	stcSystemConfig.ucSystemConfigInterPacketTx = ucDefaultInterPacketTxPeriod;
	stcSystemConfig.ucSystemConfigPercentTolerance = ucDefaultPercentTolerance;
	uiTmp = uiDefaultAddrAutoAndRemplTimeOut;
	uiTmp = uiTmp /1000;
	stcSystemConfig.ucSystemConfigLsbAddrAutoTimeOut = uiTmp;
	stcSystemConfig.ucSystemConfigMsbAddrAutoTimeOut = uiTmp >> 8;

#ifdef DALI_MASTER
	if(ucFlagTxSpi == 1)
	{
	    stcSpiArcomFrame.ucCode = SYSTEM_CONFIG;
	    memcpyST((uint8_t*)stcSpiArcomFrame.uData,(const uint8_t *)&stcSystemConfig,sizeof(stcSystemConfig_t));
	    AddCrc16ToFrame((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE);
	    Push((uint8_t *)&stcSpiArcomFrame,SPI_FRAME_SIZE,SPI_TX_FIFO);
	}
#endif
}

/******************************************************************************/
/*!\brief SetDaliGateway
 * \author Jimmy ZANOTTO
 *
 * \param       stcRuntimeDaliGateway_t *stcRuntimeDaliGateway_pt
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
void SetDaliGateway(stcRuntimeDaliGateway_t *stcRuntimeDaliGateway_pt)
{
	stcDaliTxFrame_t stcDaliTxFrameTmp = {0};

	if(stcRuntimeDaliGateway_pt->ucDaliGatewayType == DALI_GATEWAY_TYPE_ACC)
	{
		stcDaliTxFrameTmp.ucCmd = CMD_DALI_ENABLE_WRITE_MEMORY;
		stcDaliTxFrameTmp.ucDtr1 = stcRuntimeDaliGateway_pt->ucDaliGatewayDtr1;
		stcDaliTxFrameTmp.ucDtr = stcRuntimeDaliGateway_pt->ucDaliGatewayDtr0;
		stcDaliTxFrameTmp.ucLsb = stcRuntimeDaliGateway_pt->ucDaliGatewayLsb;
		stcDaliTxFrameTmp.ucMsb = stcRuntimeDaliGateway_pt->ucDaliGatewayMsb;
		stcDaliTxFrameTmp.ucAdresse = stcRuntimeDaliGateway_pt->ucDaliGatewayAddr;
		Push((uint8_t*)&stcDaliTxFrameTmp,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);
	}
	else if(stcRuntimeDaliGateway_pt->ucDaliGatewayType == DALI_GATEWAY_TYPE_BALLAST)
	{
		stcDaliTxFrameTmp.ucCmd = stcRuntimeDaliGateway_pt->ucDaliGatewayDtr1;
		stcDaliTxFrameTmp.ucDtr1 = 1;
		stcDaliTxFrameTmp.ucDtr = stcRuntimeDaliGateway_pt->ucDaliGatewayDtr0;
		stcDaliTxFrameTmp.ucLsb = 0;
		stcDaliTxFrameTmp.ucMsb = 0;
		stcDaliTxFrameTmp.ucAdresse = 0;
		Push((uint8_t*)&stcDaliTxFrameTmp,SIZE_DALI_TX_FRAME,DALI_TX_FIFO);
	}

}

/******************************************************************************/
/*!\brief DaliFsmWatchLine
 * \author JZA
 *
 * \param       void.
 *
 * \return      void.
 *
 * long description :
 *
 ******************************************************************************/
#ifdef USE_SCHEDULER
void DaliFsmWatchLine(en_change_behavior_fnt_t en_change_behavior_fnt)
#else
void DaliFsmWatchLine(void)
#endif
{

    static TICK uiLastTick_s;
    TICK uiCurrentTick_s;
    uint16_t usBytesInFifo = 0;


    switch(enDaliFsmWatchLineState)
    {
        case DALI_FSM_WATCH_LINE_INIT :
        	ucLineReady_g = DALI_LINE_NOK;
        	uiLastTick_s = tickGet();
        	HAL_GPIO_WritePin(GPIO_DALI_TX_PORT, GPIO_DALI_TX_PIN, GPIO_PIN_SET);
        	enDaliFsmWatchLineState = DALI_FSM_WATCH_LINE_FIRST_CHECK;
        break;
        case DALI_FSM_WATCH_LINE_FIRST_CHECK :
        	uiCurrentTick_s = tickGet();

        	if(uiLastTick_s > uiCurrentTick_s)
        	{/*protection*/
        		uiLastTick_s = uiCurrentTick_s;
        	}

        	if(TICK_GET_DIFF(uiCurrentTick_s,uiLastTick_s) >= TIME_LINE_OFF)
        	{
        		enDaliFsmWatchLineState = DALI_FSM_WATCH_LINE_OK;
        	}
        break;
        case DALI_FSM_WATCH_LINE_OK:
        	HAL_GPIO_WritePin(GPIO_DALI_TX_PORT, GPIO_DALI_TX_PIN, GPIO_PIN_RESET);
        	ucLineReady_g = DALI_LINE_OK;
        	enDaliFsmWatchLineState = DALI_FSM_WATCH_LINE_DO_NOTHING;
        break;
        case DALI_FSM_WATCH_LINE_DO_NOTHING:

        break;
        default:;
        break;
    }
}


