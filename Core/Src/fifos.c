/**
 * @file fifos.c
 * @author R&D Team (arcom@groupe-arcom.com)
 * @brief 
 * @version 1.0
 * @date 24-01-2020
 * 
 * @copyright Copyright Arcom 2020
 * 
 */

/* Includes ------------------------------------------------------------------*/
#include "fifos.h"

/* Private defines -----------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
//static int8_t PushByte(uint8_t byte, uint8_t fifo);
static int8_t PopByte(uint8_t *byte, uint8_t fifo, uint8_t erase);
static void InitFIFO(uint8_t fifo, uint8_t *pBase, uint16_t size);

/* Private typedef ----------------------------------------------------------*/
typedef struct
{
	uint8_t 	*pBase;
	uint16_t	size;
	uint16_t	nb_bytes;
	uint16_t	push_position;
	uint16_t	pop_position;
} fifocfng_t;

/* Private variables ---------------------------------------------------------*/
static uint8_t FIFO_DALI_TX[SIZE_FIFO_DALI_TX];
static uint8_t FIFO_DALI_RX[SIZE_FIFO_DALI_RX];

fifocfng_t	FIFOS[SIZE_FIFO];


/* Functions -----------------------------------------------------------------*/
/**
  * @brief
  * @param  none
  * @retval none
  */
void InitFIFOs(void)
{
	InitFIFO((uint8_t)DALI_TX_FIFO, (uint8_t*)&FIFO_DALI_TX, (uint16_t)sizeof(FIFO_DALI_TX));
	InitFIFO((uint8_t)DALI_RX_FIFO, (uint8_t*)&FIFO_DALI_RX, (uint16_t)sizeof(FIFO_DALI_RX));
}
/**
  * @brief
  * @param  none
  * @retval none
  */
int8_t Push(uint8_t *data, uint16_t size, uint8_t fifo)
{
	uint8_t ret = 0;
	uint16_t i = 0;
	uint8_t push_data;
	

	if(size > (FIFOS[fifo].size - FIFOS[fifo].nb_bytes))
		return -1;		
	do
	{
		push_data = *(data + i);
		ret = PushByte(push_data, fifo);
		i++;
	} while(!ret && i < size);
	
	return ret;
}
/**
  * @brief
  * @param  none
  * @retval none
  */
int8_t Pop(uint8_t *data, uint16_t size, uint8_t fifo)
{
	uint8_t ret = 0;
	uint16_t i = 0;
	uint8_t pop_data;

	if(size > FIFOS[fifo].nb_bytes)
		return -1;
		
	do
	{
		ret = PopByte(&pop_data, fifo, 1);
		if(!ret)
		{
			*(data + i) = pop_data;
			i++;		
		}
	} while(!ret && i < size);
	
	return ret;
}
/**
  * @brief
  * @param  none
  * @retval none
  */
int8_t PushByte(uint8_t byte, uint8_t fifo)
{

	if(FIFOS[fifo].nb_bytes == FIFOS[fifo].size)
		return -1;

	FIFOS[fifo].nb_bytes++;

	*(FIFOS[fifo].pBase + FIFOS[fifo].push_position) = byte;

	FIFOS[fifo].push_position++;
	if(FIFOS[fifo].push_position >= FIFOS[fifo].size)
		FIFOS[fifo].push_position = 0;

	return 0;
}
/**
  * @brief
  * @param  none
  * @retval none
  */
int8_t PopByte(uint8_t *byte, uint8_t fifo, uint8_t erase)
{

	if(!FIFOS[fifo].nb_bytes)
		return -1;

	*byte = *(FIFOS[fifo].pBase + FIFOS[fifo].pop_position);

	if(erase)
		*(FIFOS[fifo].pBase + FIFOS[fifo].pop_position) = 0x0;

	FIFOS[fifo].nb_bytes--;
	FIFOS[fifo].pop_position++;
	if(FIFOS[fifo].pop_position >= FIFOS[fifo].size)
		FIFOS[fifo].pop_position = 0;

	return 0;
}

/**
  * @brief
  * @param  none
  * @retval none
  */
int8_t ReadPop(uint8_t *data, uint16_t size, uint8_t fifo)
{
	uint8_t ret = 0;
	uint16_t i = 0;
	uint8_t pop_data;
	uint16_t	last_nb_bytes;
	int16_t	last_pop_position;

	if(size > FIFOS[fifo].nb_bytes)
		return -1;
			
	last_nb_bytes = FIFOS[fifo].nb_bytes;
	last_pop_position = FIFOS[fifo].pop_position;
	
	do
	{
		ret = PopByte(&pop_data, fifo, 0);
		if(!ret)
		{
			*(data + i) = pop_data;
			i++;		
		}
	} while(!ret && i < size);

	FIFOS[fifo].nb_bytes = last_nb_bytes;
	FIFOS[fifo].pop_position = last_pop_position;
	
	return ret;
}
/**
  * @brief
  * @param  none
  * @retval none
  */
uint16_t GetFifoNbBytes(uint8_t fifo)
{
	return 	FIFOS[fifo].nb_bytes;
}
/**
  * @brief
  * @param  none
  * @retval none
  */
void InitFIFO(uint8_t fifo, uint8_t *pBase, uint16_t size)
{
	uint16_t i = 0;
	
	FIFOS[fifo].pBase = pBase;
	FIFOS[fifo].nb_bytes = 0;
	FIFOS[fifo].push_position = 0;
	FIFOS[fifo].pop_position = 0;
	FIFOS[fifo].size = size;	

	for(i = 0; i < size; i++)
		*(FIFOS[fifo].pBase + i) = 0x0;
}
/**
  * @brief
  * @param  none
  * @retval none
  */
void ClearFIFO(uint8_t fifo)
{		
	uint16_t i;
	
	FIFOS[fifo].nb_bytes = 0;
	FIFOS[fifo].push_position = 0;
	FIFOS[fifo].pop_position = 0;
	
	for(i = 0; i < FIFOS[fifo].size; i++)
		*(FIFOS[fifo].pBase + i) = 0x0;
	
}

