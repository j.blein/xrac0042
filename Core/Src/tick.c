/**
 * @file tick.c
 * @author R&D Team (arcom@groupe-arcom.com)
 * @brief 
 * @version 1.0
 * @date 06-12-2019
 * 
 * @copyright Copyright Arcom 2019
 * 
 */
#include "tick.h"
/* Global variables -------------------------------------------*/

/* External variables -----------------------------------------*/
extern __IO uint32_t uwTick;

/* Private defines --------------------------------------------*/

/* Private typedef --------------------------------------------*/

/* Private macros ---------------------------------------------*/

/* Private variables ------------------------------------------*/

/* Private function prototype ---------------------------------*/

/**
 * @brief Obtains the current Tick value.
 * 
 * @return uint32_t 32 bits of the current Tick value
 */
uint32_t tickGet(void)
{
    return uwTick;
}

/**
 * @brief Used for testing systick (attention this a blocking function)
 * 
 * @param timeout 
 */
void tickSleep(uint32_t timeout)
{
    uint32_t lastSleepTick = 0;
    uint32_t currentTick;
    lastSleepTick = uwTick;

    do {
        currentTick = uwTick;
        currentTick = TICK_GET_DIFF(currentTick, lastSleepTick);
    } while (currentTick <= timeout);
}
