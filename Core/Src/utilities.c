/**
 * @file utilities.c
 * @author R&D Team (arcom@groupe-arcom.com)
 * @brief 
 * @version 1.0
 * @date 27-01-2020
 * 
 * @copyright Copyright Arcom 2020
 * 
 */
#include "utilities.h"
#include "config.h"


/* Global variables -------------------------------------------*/

/* External variables -----------------------------------------*/

/* Private defines --------------------------------------------*/

/* Private typedef --------------------------------------------*/

/* Private macros ---------------------------------------------*/

/* Private variables ------------------------------------------*/

/* Private function prototype ---------------------------------*/
static uint16_t CalcByteCrc(uint8_t uCSerData, uint16_t usLastCrc);

/**
 * @brief This function is executed in case of error occurrence
 * 
 */
//void Error_Handler(void)
//{
//    while (1)
//    {
//    	//TODO Watchdog maybe
//    }
//}


/**
  * @brief  Compares two buffers.
  * @param  pBuffer1, pBuffer2: buffers to be compared.
  * @param  BufferLength: buffer's length
  * @retval true  : pBuffer1 identical to pBuffer2
  *         false : pBuffer1 differs from pBuffer2
  */
bool Buffercmp(uint8_t *pBuffer1, uint8_t *pBuffer2, uint16_t BufferLength)
{
 
    while (BufferLength--)
    {
        if ((*pBuffer1) != *pBuffer2)
        {
            return false;
        }
        pBuffer1++;
        pBuffer2++;
    }
        return true;
}


/**
 * @brief Copies size elements of src array to dst array
 *        STM32 Standard memcpy function only works on pointers that are aligned
 * @param dst   Destination array
 * @param src   Source array
 * @param size  Size Number of bytes to be copied
 */
void memcpyST( uint8_t *ucDst_p, const uint8_t *ucSrc_p, uint16_t usSize )
{
    while( usSize-- )
    {
        *ucDst_p++ = *ucSrc_p++;
    }
}

/**
 * @brief CRC with the CCITT 16bit algorithm (X^16 + X^12 + X^5 + 1)
 * 
 * @param ser_data 
 * @param last_crc 
 * @return uint16_t 
 */
static uint16_t CalcByteCrc(uint8_t uCSerData, uint16_t usLastCrc)
{
    uint16_t usCrc;

    usCrc = usLastCrc;

    usCrc  = (uint8_t)(usCrc >> 8) | (usCrc << 8);
    usCrc ^= uCSerData;
    usCrc ^= (uint8_t)(usCrc & 0xff) >> 4;
    usCrc ^= (usCrc << 8) << 4;
    usCrc ^= ((usCrc & 0xff) << 4) << 1;

    return usCrc;
}

/**
 * @brief CRC calcule of arcom data frame 
 * 
 * @param spiArcomFr_p pointer to structure of type stcSPIArcomFrame_t
 * @return uint16_t 
 */
uint16_t CalcCrc16Frame(uint8_t *uc_p, uint16_t usFrSize)
{
    uint16_t usCrc = 0, i = 0;
    uint8_t ucData;

    for(i = 0; i < usFrSize; i++)
    {
        ucData = *uc_p++;
        usCrc = CalcByteCrc(ucData, usCrc);
    }

    return usCrc; 
}

/**
 * @brief Get the My Endianness object
 * 
 * @return true   -> little endian
 * @return false  -> big endian
 */
bool getMyEndianness(void)
{
    bool bRet = false;
    uint32_t data;
    uint8_t *cptr;

    data = 1; 
    cptr = (uint8_t *)&data; 

    if (*cptr == 1)
    {
        bRet = true;
    }
    else if (*cptr == 0)
    {
        bRet = false;
    }

    return bRet;
}

/**
 * @brief 
 * 
 * @param spiArcomFr_p 
 */
void AddCrc16ToFrame(uint8_t * ucData_p,uint16_t usSize)
{
    uint16_t usCrc = 0, i = 0;
    uint8_t ucData;
    uint8_t *ucTmp_p = ucData_p;
    uint8_t ucMSBTemp;
    uint8_t ucLSBTemp;
    uint16_t usCrc16;
    uint16_t *usCrc16_p = (uint16_t *)(ucTmp_p + usSize - sizeof(uint16_t));

    usCrc16 = CalcCrc16Frame(ucTmp_p, usSize - sizeof(uint16_t));

    ucMSBTemp = ((usCrc16 & 0xFF00) >> 8);
    ucLSBTemp = usCrc16 & 0x00FF;

    *usCrc16_p =  (uint16_t)(ucMSBTemp) | (uint16_t)(ucLSBTemp << 8);

}
